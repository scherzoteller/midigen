package org.scherzoteller.midigen;

import static org.scherzoteller.midigen.midiengine.AvailableInterfaces.GERVILL;
import static org.scherzoteller.midigen.midiengine.AvailableInterfaces.SKULPT;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.gui.MidiGenGUI;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiInstruments;
import org.scherzoteller.midigen.midiengine.ScherzoMidiDevice;
import org.scherzoteller.midigen.play.CompositePlayer;
import org.scherzoteller.midigen.play.Player;
import org.scherzoteller.midigen.processing.ProcessingApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import processing.core.PApplet;

public class MidiGen implements MidiInstruments {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MidiGen.class);
	private static final Logger ORCHESTRA = LoggerFactory.getLogger("ORCHESTRA");
	

	private static class ScherzoMidiChannel{
		public int lastMidiChannel = -1;
		public String[] midiLabels = new String[16];
	}

	private static EnumMap<AvailableInterfaces, ScherzoMidiChannel> MIDI_CHANNELS = new EnumMap<>(AvailableInterfaces.class);
	
	static{
		MIDI_CHANNELS.put(GERVILL, new ScherzoMidiChannel());
		MIDI_CHANNELS.put(SKULPT, new ScherzoMidiChannel());
	}
	
	
	
	public static synchronized int assignChannel(String label, AvailableInterfaces iface){
		ScherzoMidiChannel channel = MIDI_CHANNELS.get(iface);
		channel.lastMidiChannel++;
		channel.midiLabels[channel.lastMidiChannel] = label;
		return channel.lastMidiChannel;
	}
	

	private static void startOrchestra(){
		ORCHESTRA.info(" ▄██████▄     ▄████████  ▄████████    ▄█    █▄       ▄████████    ▄████████     ███        ▄████████    ▄████████ ");
		ORCHESTRA.info("███    ███   ███    ███ ███    ███   ███    ███     ███    ███   ███    ███ ▀█████████▄   ███    ███   ███    ███ ");
		ORCHESTRA.info("███    ███   ███    ███ ███    █▀    ███    ███     ███    █▀    ███    █▀     ▀███▀▀██   ███    ███   ███    ███ ");
		ORCHESTRA.info("███    ███  ▄███▄▄▄▄██▀ ███         ▄███▄▄▄▄███▄▄  ▄███▄▄▄       ███            ███   ▀  ▄███▄▄▄▄██▀   ███    ███ ");
		ORCHESTRA.info("███    ███ ▀▀███▀▀▀▀▀   ███        ▀▀███▀▀▀▀███▀  ▀▀███▀▀▀     ▀███████████     ███     ▀▀███▀▀▀▀▀   ▀███████████ ");
		ORCHESTRA.info("███    ███ ▀███████████ ███    █▄    ███    ███     ███    █▄           ███     ███     ▀███████████   ███    ███ ");
		ORCHESTRA.info("███    ███   ███    ███ ███    ███   ███    ███     ███    ███    ▄█    ███     ███       ███    ███   ███    ███ ");
		ORCHESTRA.info(" ▀██████▀    ███    ███ ████████▀    ███    █▀      ██████████  ▄████████▀     ▄████▀     ███    ███   ███    █▀  ");
		ORCHESTRA.info("             ███    ███                                                                   ███    ███              ");
	}

	public static void piece1(Map<String, PlayerAction> playerActions, long duration, Generator generator) throws InterruptedException {
		piece1(playerActions, true, generator, Optional.empty());
	}

	
	public static ExecutorService executorService;
	
	
	
	
	/**
	 * 
	 * @param awaitTermination if true will wait for termination of all threads from executor service (don't use if called from AWT evt thread to avoid dead lock!)
	 * @param generator 
	 * @param action
	 * @throws InterruptedException
	 */
	public static void piece1(Map<String, PlayerAction> playerActions, boolean awaitTermination, Generator generator, Optional<Consumer<CompositePlayer>> action) throws InterruptedException {
		System.err.println("piece1");
		startOrchestra();
		
		List<Player> players = new ArrayList<>(4);
		
		System.err.println("Creating thread queue");
		executorService = new ThreadPoolExecutor(playerActions.size()+3, playerActions.size()+3, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		playerActions.forEach((threadName, playerAction) -> {
			System.err.println("Adding "+threadName);
			executorService.submit(() -> {
				Thread.currentThread().setName(threadName);
				playerAction.action(players, generator);
				System.err.println(threadName+" ended");
			});
			System.err.println("Added "+threadName);
		});
		
		
		while(players.size()<playerActions.size()){
			System.err.println("@@@@@@@@@@ ************* We have "+players.size()+"/"+playerActions.size()+" players");
			ORCHESTRA.info("@@@@@@@@@@ ************* We have "+players.size()+"/"+playerActions.size()+" players");
			Thread.sleep(100);
		}
		System.err.println("*$*$*$*$*$*$*$*$*$ We have all players!");
		ORCHESTRA.info("*$*$*$*$*$*$*$*$*$ We have all players!");
		System.err.println("@@@@@@@@@@ ************* We have "+players.size()+" players");
		ORCHESTRA.info("@@@@@@@@@@ ************* We have "+players.size()+" players");
		
		if(action.isPresent()) {
			action.get().accept(new CompositePlayer(players));
		}
//		if(awaitTermination) {
//			executorService.shutdown();
//			executorService.awaitTermination(300, TimeUnit.SECONDS);
//			Thread.sleep(500);// let the last notes resonate
//		}
	}
	
	private static Player mono(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int instrument, Generator generator) {
		try (ScherzoClassicMonoPlayer player = new ScherzoClassicMonoPlayer(MidiGen.getConfig(), iface, assignChannel(instrumentLabel, iface), instrument, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("mono: "+e, e);
			throw new RuntimeException(e);
		}
	}
	
	private static Player polyOrc(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int[] instruments, Generator generator) {
		try (ScherzoClassicPolyOrchestraPlayer player = new ScherzoClassicPolyOrchestraPlayer(MidiGen.getConfig(),iface, assignChannel(instrumentLabel, iface), true, instrumentLabel, instruments, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("polyorc: "+e, e);
			throw new RuntimeException(e);
		}
	}
	
	private static Player poly(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int instrument, Generator generator) {
		try (ScherzoClassicPolyPlayer player = new ScherzoClassicPolyPlayer(MidiGen.getConfig(), iface, assignChannel(instrumentLabel, iface), true, instrumentLabel, instrument, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("poly: "+e, e);
			throw new RuntimeException(e);
		}
	}

	private static Player serial(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int instrument, Generator generator) {
		try (ScherzoSerialPolyPlayer player = new ScherzoSerialPolyPlayer(MidiGen.getConfig(), iface, assignChannel(instrumentLabel, iface), true, instrumentLabel, instrument, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("serial: "+e, e);
			throw new RuntimeException(e);
		}
	}

	private static Player drone(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int instrument, Generator generator, boolean skulpt) {
		try (ScherzoPolyDronePlayer player = new ScherzoPolyDronePlayer(MidiGen.getConfig(), iface, assignChannel(instrumentLabel, iface), instrumentLabel, instrument, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("serial: "+e, e);
			throw new RuntimeException(e);
		}
	}

	private static Player drone(AvailableInterfaces iface, Collection<Player> players, String instrumentLabel, int[] instruments, Generator generator, boolean skulpt) {
		try (ScherzoPolyDronePlayer player = new ScherzoPolyDronePlayer(MidiGen.getConfig(), iface, assignChannel(instrumentLabel, iface), instrumentLabel, instruments, generator)) {
			if(players != null) players.add(player);
			player.play();
			return player;
		} catch (Exception e) {
			LOGGER.error("serial: "+e, e);
			throw new RuntimeException(e);
		}
	}
	
	
	public static void piece1Gui(Map<String, PlayerAction> playerActions, boolean awaitTermination) {
		try {
			MidiGenGUI gui = new MidiGenGUI();
			piece1(playerActions, awaitTermination, gui, Optional.of(player -> {
				gui.addPlayer(player);
			}));
		} catch (Exception e) {
			LOGGER.error("piece1Gui: "+e, e);
			throw new RuntimeException(e);
		}
	}
	
	
	private static interface PlayerAction{
		void action(List<Player> players, Generator generator);
	}
	
	public static File PERF_FILE = null;
	public static void main(String[] args) throws Exception {
		File perfFile;
		if(args.length > 0 && (perfFile = new File(args[0])).exists()) {
			PERF_FILE = perfFile;
		}else{
			PERF_FILE = new File("./performances.json");
			if(!PERF_FILE.exists()){
				PERF_FILE.createNewFile();
			}
		}
//		PApplet.main(ProcessingApp.class.getName());
		ScherzoMidiDevice.debug();
		piece1GuiWithSelector();
	}

	private static MidiGenConfig config = null;
	
	
	public static synchronized MidiGenConfig getConfig(){
		return config;
	}
	public static synchronized void setConfig(MidiGenConfig config){
		MidiGen.config = config;
	}
	
	private static void piece1GuiWithSelector() {
		new Thread(() ->  {
			Thread.currentThread().setName("MAIN THREAD");
			while(getConfig() == null)
				;
			
			Map<String, PlayerAction> playerActions = new LinkedHashMap<>();

			if(getConfig().isHasPiano())
				playerActions.put("PIANO MainThread", (players, generator) -> poly(AvailableInterfaces.GERVILL, players, "PIANO", PIANO_ACOUSTIQUE_DE_CONCERT, generator));
			if(getConfig().isHasPercussions())
				playerActions.put("PERCUSSIONS MainThread", (players, generator) -> polyOrc(AvailableInterfaces.GERVILL, players, "PERCUSSIONS", PERCUSSIONS, generator));
			if(getConfig().isHasSoftSynth())
				playerActions.put("SOFT_SYNTH MainThread", (players, generator) -> polyOrc(AvailableInterfaces.GERVILL, players, "SOFT_SYNTH", SOFT_SYNTH, generator));
			if(getConfig().isHasLeadSynth())
				playerActions.put("SYNTH_LEAD MainThread", (players, generator) -> mono(AvailableInterfaces.GERVILL, players, "SYNTH_LEAD", ONDE_DENT_DE_SCIE, generator));
			if(getConfig().isHasSerial())
				playerActions.put("SERIAL MainThread", (players, generator) -> serial(AvailableInterfaces.GERVILL, players, "SERIAL", PIANO_ACOUSTIQUE_DE_CONCERT, generator));
			
			
			if(getConfig().isHasDrone())
				playerActions.put("SKULPT_DRONE MainThread", (players, generator) -> drone(AvailableInterfaces.SKULPT, players, "SKULPT_DRONE", ScherzoPolyDronePlayer.GOOD_SKULPT_DRONES, generator, false));
			if(getConfig().isHasCelesta())
				playerActions.put("CELESTA MainThread", (players, generator) -> mono(AvailableInterfaces.GERVILL, players, "CELESTA", CELESTA, generator));
			if(getConfig().isHasGervillDrone())
				playerActions.put("GERVILL_DRONE MainThread", (players, generator) -> drone(AvailableInterfaces.GERVILL, players, "GERVILL_DRONE", CORDES_SYNTHETIQUES_1, generator, false));
			if(getConfig().isHasSkulptBeat())
				playerActions.put("SKULPT_BEAT MainThread", (players, generator) -> polyOrc(AvailableInterfaces.SKULPT, players, "SKULPT_BEAT", ScherzoClassicPolyOrchestraPlayer.SKULPT_PERCUSSIONS, generator));
			
			
			piece1Gui(playerActions, true);
		}).start();
		MidiGenGUI.selectDevice((cfg)  -> {
			// Avoid doing to many stuffs in the awt event thread to prevent dead locks
			setConfig(cfg);
		});
	}
	

}
