package org.scherzoteller.midigen;

import javax.sound.midi.MidiDevice;

import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.ScherzoMidiDevice;
import org.scherzoteller.midigen.play.Player;

public abstract class ScherzoAbstractPlayer<D> extends ScherzoMidiDevice<D> implements Player {
	private static final int[] VELOCITIES = {55, 64, 72, 84};
	private static final String[] VELOCITIES_NAMES = {"p", "mf", "f", "ff"};
	protected int currentVelocity = 64;
	protected final Generator generator;
	
	public ScherzoAbstractPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, Generator generator) {
		super(config, iface, channel);
		this.generator = generator;
	}
	
//	public ScherzoAbstractPlayer(MidiDevice devSynth, MidiDevice devSeq, int channel, Generator generator) {
//		super(devSynth, devSeq, channel);
//		this.generator = generator;
//	}
//
//	public ScherzoAbstractPlayer(String synthDevice, String seqDevice, int channel, Generator generator) {
//		super(synthDevice, seqDevice, channel);
//		this.generator = generator;
//	}
	
	protected synchronized void changeVelocity(){
		if(generator.getBoolean(3)){
			int veloIdx = generator.getInt() % VELOCITIES.length;
			ORCHESTRA.info("Change velocity... => "+VELOCITIES_NAMES[veloIdx]);
			this.currentVelocity = VELOCITIES[veloIdx];
		}
	}
	
	protected abstract void initPlay();
	protected abstract long playIteration(long timePlayed, long timeToPlay);
	protected void endPlay(){
		// Nothing by default
	}
	
	private boolean initialized = false;
	private boolean shouldPlay = false;
	
	
	
	public synchronized boolean isShouldPlay() {
		return shouldPlay;
	}

	public synchronized void setShouldPlay(boolean shouldPlay) {
		this.shouldPlay = shouldPlay;
	}

	public void play() {
		synchronized (this) {
			if(!initialized){
				initPlay();
				initialized = true;
			}
		}
		while(true){
			if(!isShouldPlay()){
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					LOGGER.error(e.toString(), e);
					throw new RuntimeException(e);
				}
			}else{
				playIteration(0, Integer.MAX_VALUE);
			}
		}
	}
	
	@Override
	public synchronized void start() {
		setShouldPlay(true);
	}
	
	@Override
	public synchronized void pause() {
		setShouldPlay(false);
	}

	@Override
	public void reset() {
		initPlay();
	}
	
}
