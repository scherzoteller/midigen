package org.scherzoteller.midigen;

import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiNotes;

public class ScherzoClassicMonoPlayer extends ScherzoAbstractPlayer<PolyPlayerData> implements MidiNotes {
	private int[] currentScale;
	//private static final int[] LENGTH = {200, 400, 800, 1800};
	private static final int[] LENGTH = {200, 400};
	
	public ScherzoClassicMonoPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, int instrument, Generator generator) {
		super(config, iface, channel, generator);
		changeInstrument(instrument);
	}

	private void changeScale(long number){
		int scaleIdx = (int)(number % NB_SCALES);
		this.currentScale = SCALES[scaleIdx];
		ORCHESTRA.info("... => "+SCALE_NAMES[scaleIdx]);

	}
	
	private void initScaleIfNeeded(){
		if(currentScale == null){
			ORCHESTRA.info("Init scale");
			changeScale(generator.getLong());
		}
	}

	@Override
	public void play() {
		super.play();
		System.err.println("play!");
	}
	
	@Override
	public synchronized void start() {
		super.start();
		System.err.println("play!");
	}
	
	@Override
	protected synchronized void initPlay() {
		initScaleIfNeeded();
	}
	
	@Override
	protected synchronized long playIteration(long timePlayed, long timeToPlay) {
		if(generator.getBoolean(4)){
			changeScale(generator.getLong());
		}
		changeVelocity();
		
		int noteIdx = generator.getInt()%currentScale.length;
		
		int noteLength = LENGTH[generator.getInt()%LENGTH.length];
		
		if(timePlayed + noteLength > timeToPlay){
			noteLength = (int)(timeToPlay - timePlayed);
			timePlayed = timeToPlay;
		}else{
			timePlayed +=noteLength;
		}
		
		playNote(currentScale[noteIdx], currentVelocity, noteLength, new PolyPlayerData(currentScale, noteIdx));
		return timePlayed;
	}
}
