package org.scherzoteller.midigen;

import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiInstruments;

public class ScherzoClassicPolyOrchestraPlayer extends ScherzoClassicPolyPlayer implements MidiInstruments {
	// TODO some percs for Skulpt
	
	private int[] instruments;
	
	public ScherzoClassicPolyOrchestraPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, boolean useChords, String instrumentName, int[] instruments, Generator generator) {
		super(config, iface, channel, useChords, instrumentName, -1, generator);
		this.instruments = instruments;
	}

	public ScherzoClassicPolyOrchestraPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, boolean useChords, String instrumentName, int instrument, Generator generator) {
		super(config, iface, channel, useChords, instrumentName, instrument, generator);
	}

	public ScherzoClassicPolyOrchestraPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, Generator generator) {
		super(config, iface, channel, generator);
	}

	private long max(long l1, long l2){
		return l1 >= l2 ? l1 : l2;
	}
	
	@Override
	protected long playNote(long timePlayed, long timeMilis) {
		long noteLength = -1;
		for(int instr: instruments){
			if(generator.getBoolean(1)){
				changeInstrument(instr);
				noteLength = max(noteLength, super.playNote(timePlayed, timeMilis));
			}
		}
		return super.playNote(timePlayed, timeMilis);
	}
	
}
