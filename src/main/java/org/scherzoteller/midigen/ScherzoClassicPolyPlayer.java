package org.scherzoteller.midigen;

import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiNotes;

public class ScherzoClassicPolyPlayer extends ScherzoAbstractPlayer<PolyPlayerData> implements MidiNotes {
	private static final boolean WAIT_NOTE_END_BEFORE_SCALE_CHANGE = true;
	private static int MAX_NOTE_TO_TRIGGER_CHORDS = 2; // -1 for unlimited 
	private int[] currentScale;
	private boolean useChords = false;
//	private static final int[] LENGTH = {200, 400, 800, 1800, 2400, 3600};
	private static final int[] LENGTH = {100, 200, 200, 400, 800, 1800}; // , 1800
//	private static final int[] LENGTH = {200, 400};
	//private static final int[] VELOCITIES = {36, 48, 64, 72, 84};
	
	private Set<Integer> playingNotes = new TreeSet<>();
	
	private String instrumentName = "DEFAULT" ;
	
	public ScherzoClassicPolyPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, Generator generator) {
		super(config, iface, channel, generator);
	}
	
	public ScherzoClassicPolyPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, boolean useChords, String instrumentName , int instrument, Generator generator) {
		super(config, iface, channel, generator);
		if(instrument>=0){
			changeInstrument(instrument);
		}
		this.useChords = useChords;
		this.instrumentName = instrumentName;
	}
	
	private synchronized void changeScale(long number){
		int scaleIdx = (int)(number % NB_SCALES);
		this.currentScale = SCALES[scaleIdx];
		ORCHESTRA.info("... => "+SCALE_NAMES[scaleIdx]);

	}
	
	private void initScaleIfNeeded(){
		if(currentScale == null){
			ORCHESTRA.info("Init scale");
			changeScale(generator.getLong());
		}
	}
	
	private void printPolyphony(){
		synchronized(playingNotes){
			StringBuilder sbStars = new StringBuilder();
			for(int i=0;i<playingNotes.size();i++){
				sbStars.append('*');
			}
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.leftPad(sbStars.toString(), 10, ' ')).append(' ').append(playingNotes.stream().map(noteIdx -> NOTE_NAMES[noteIdx] ).collect(Collectors.joining(", ")));
			
			if(sb.toString().contains("null")){
				System.err.println("WHAT?");
			}
			
			ORCHESTRA.info(sb.toString());
		}
	}
	
	@Override
	public void playNote(int note, int velocity, int durationMs, PolyPlayerData data) { // , int[] currentScale, int scaleIdx
		playNote(note, velocity, durationMs, useChords, data);
	}
	
	private boolean canTriggerChords(){
		if(MAX_NOTE_TO_TRIGGER_CHORDS < 0){
			return true;
		}else{
			synchronized (playingNotes) {
				return playingNotes.size() <= MAX_NOTE_TO_TRIGGER_CHORDS;
			}
		}
	}
	
	private static final boolean CHORDS_IN_SCALE = true;
	
	public void playNote(int note, int velocity, int durationMs, boolean useChords, PolyPlayerData data) {
		if(note>C8){
			ORCHESTRA.info("Skip note over max, CHORD will be truncated ");
			return;
		}
		synchronized(playingNotes){
			if(playingNotes.contains(note)) {
				ORCHESTRA.info("Skip duplicated");
				return;
			}
		}
		
		
		// TODO Thread pool!!!!!
		new Thread(() -> {
			synchronized(playingNotes){
				playingNotes.add(note);
			}
			
			printPolyphony();
			super.playNote(note, velocity, durationMs, data);
			if(useChords && canTriggerChords()){
				
				boolean seventh = generator.getBoolean(1);
				boolean seventhMinor = !seventh && generator.getBoolean(1);
				if(CHORDS_IN_SCALE){
					// note rank in chord: 
					// - 0: fundamental 
					// - 1: 3rd 
					// - 2: 5th 
					// - 3: 7th
					// Above: no chord
					int noteRank = generator.getInt(10);
					
					Consumer<Integer> rankOperation = r -> {
						int targetIdx = data.scaleIdx + r;
						if(targetIdx < data.currentScale.length && targetIdx >=0 ) playNote(currentScale[targetIdx], velocity, durationMs, false, null);
					};
					
					switch(noteRank){
					case 0:
						rankOperation.accept(2);
						rankOperation.accept(4);
						if(generator.getBoolean(2)){
							rankOperation.accept(6);
						}
						break;
					case 1:
						rankOperation.accept(-2);
						rankOperation.accept(2);
						if(generator.getBoolean(2)){
							rankOperation.accept(4);
						}
						break;
					case 2:
						rankOperation.accept(-4);
						rankOperation.accept(-2);
						if(generator.getBoolean(2)){
							rankOperation.accept(2);
						}
					case 3:
						rankOperation.accept(-6);
						rankOperation.accept(-4);
						rankOperation.accept(-2);
						break;
					}
					
				}else{
					if(generator.getBoolean(2)){
						// MAJOR
						ORCHESTRA.info(">>>> MAJOR CHORD "+NOTE_NAMES[note]+(seventh?"7":"")+(seventhMinor?"7m":""));
						playNote(note+4, velocity, durationMs, false, null);
						playNote(note+7, velocity, durationMs, false, null);
						if(seventh){
							playNote(note+10, velocity, durationMs, false, null);
						}
						if(seventhMinor){
							playNote(note+11, velocity, durationMs, false, null);
						}
					}else if(generator.getBoolean(2)){
						// MINOR
						ORCHESTRA.info(">>>> MINOR CHORD "+NOTE_NAMES[note]+"m"+(seventh?"7":"")+(seventhMinor?"7m":""));
						playNote(note+3, velocity, durationMs, false, null);
						playNote(note+7, velocity, durationMs, false, null);
						if(seventh){
							playNote(note+10, velocity, durationMs, false, null);
						}
						if(seventhMinor){
							playNote(note+11, velocity, durationMs, false, null);
						}
					}
					
				}
			}
			synchronized(playingNotes){
			playingNotes.remove(note);
			}
		}, instrumentName+" NoteThread").start();
	}
	
	
	
	@Override
	protected void endPlay() {
		waitForNotesToEnd();
	}
	
	@Override
	protected void initPlay() {
		initScaleIfNeeded();
	}
	
	@Override
	protected long playIteration(final long timePlayed, final long timeToPlay) {
		long newTimePlayed = timePlayed + playNote(timePlayed, timeToPlay);
		try {
			Thread.sleep(LENGTH[generator.getInt()%LENGTH.length]);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		return newTimePlayed;
	}

	protected long playNote(final long timePlayed, final long timeToPlay) {
		if(generator.getBoolean(4)){
			if(WAIT_NOTE_END_BEFORE_SCALE_CHANGE){
				waitForNotesToEnd();
			}
			changeScale(generator.getLong());
		}
		changeVelocity();
		
		
		int noteIdx = generator.getInt()%currentScale.length;
		
		if(NOTE_NAMES[currentScale[noteIdx]].contains("ERR#")){
			ORCHESTRA.error("Invalid note index => "+currentScale[noteIdx] + " => "+NOTE_NAMES[currentScale[noteIdx]]);
		}else if(NOTE_NAMES[currentScale[noteIdx]] == null){
			ORCHESTRA.error("Invalid note index => "+currentScale[noteIdx] + " => "+NOTE_NAMES[currentScale[noteIdx]]);
		}
		
		int noteLength = LENGTH[generator.getInt()%LENGTH.length];
		
		if(timePlayed + noteLength > timeToPlay){
			noteLength = (int)(timeToPlay - timePlayed);
//			timePlayed = timeToPlay;
//		}else{
//			timePlayed +=noteLength;
		}
		
		playNote(currentScale[noteIdx], currentVelocity, noteLength, new PolyPlayerData(currentScale, noteIdx));
		return noteLength;
	}

	private boolean notesPlaying(){
		synchronized (playingNotes) {
			return !playingNotes.isEmpty();
		}
	}
	
	private void waitForNotesToEnd() {
		ORCHESTRA.info("Waiting for notes to end...");
		while(notesPlaying()){
			try {
				printPolyphony();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
	@Override
	public void hardReset() {
		killAllNotes();
	}
	
	private void killAllNotes() {
		synchronized (playingNotes) {
			playingNotes.forEach( note -> sendNoteOff(note));
			playingNotes.clear();
		}
	}
	

}
