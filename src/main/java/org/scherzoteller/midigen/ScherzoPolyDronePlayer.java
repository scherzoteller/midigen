package org.scherzoteller.midigen;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiNotes;
import org.scherzoteller.midigen.midiengine.ScherzoMidiDevice;


public class ScherzoPolyDronePlayer extends ScherzoAbstractPlayer<RunningSerie> implements MidiNotes, SkulptMidiMapping {
	private static final int VELOCITY = 30;
	private static final int MAX_WAIT = 5000;
	private static final int[] LENGTH = {8000, 12000, 18000, 24000}; // , 1800
	private List<Integer> playingNotes = new ArrayList<>(MAX_POLYPHONY);
	private List<Integer> pausedNotes = new ArrayList<>(MAX_POLYPHONY);
	private List<RunningSerie> currentSeries = new ArrayList<>(MAX_POLYPHONY);
	
	// some percs
	public static final int[] GOOD_SKULPT_DRONES = {15,10,12,16};
	public int[] orchestra;
	

//	public ScherzoPolyDronePlayer(MidiGenConfig config, int channel, Generator generator) {
//		super(config,channel, generator);
//	}
	
	public ScherzoPolyDronePlayer(MidiGenConfig config,AvailableInterfaces iface, int channel, String instrumentName, int instrument , Generator generator) {
		super(config,iface,channel, generator);
		if(instrument>0){
			changeInstrument(instrument);
		}
	}
	
	public ScherzoPolyDronePlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, String instrumentName, int[] orchestra , Generator generator) {
		super(config,iface, channel, generator);
		this.orchestra = orchestra;
	}
	
	private void printPolyphony(){
		synchronized(playingNotes){
			StringBuilder sbStars = new StringBuilder();
			for(int i=0;i<playingNotes.size();i++){
				sbStars.append('*');
			}
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.leftPad(sbStars.toString(), 10, ' ')).append(' ').append(playingNotes.stream().map(noteIdx -> NOTE_NAMES[noteIdx] ).collect(Collectors.joining(", ")));
			
			if(sb.toString().contains("null")){
				System.err.println("WHAT?");
			}
			
			ORCHESTRA.info(sb.toString());
		}
	}
	
	
	@Override
	protected void endPlay() {
		waitForNotesToEnd();
	}
	
	@Override
	public synchronized void pause() {
		super.pause();
		pausedNotes.addAll(playingNotes);
		killAllNotes();
	}
	
	
	private int findAValidNote() {
		// 108-21+1 = 88
		// [0-87]
		// 21 + [0-87] = [21-108]
		return generator.getInt(D8_FLAT_FLAT - A0 + 1)+A0;
	}
	
	
	@Override
	protected void initPlay() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
		}
		changeInstrument();
		
	}

	private void setDrony() {
		sendCc(SKULPT_CC_AEG_ATTACK, 64); // Amp EG Attack
		sendCc(SKULPT_CC_AEG_RELEASE, 64); // Amp EG Release
	}

	private void playNotes() {
		for(int i=0; i<ScherzoMidiDevice.MAX_POLYPHONY; i++) {
			int note = findAValidNote();
			sendNoteOn(note, VELOCITY);
			synchronized (playingNotes) {
				playingNotes.add(note);
			}
		}
		printPolyphony();
	}

	@Override
	public void reset() {
		killAllNotes();
		pausedNotes.clear();
	}
	
	private void changeNote(int idx) {
		Integer old = playingNotes.get(idx);
		int newa = findAValidNote();
		playingNotes.set(idx, newa);
		sendNoteOff(old);
		sendNoteOn(newa, VELOCITY);
		
	}
	
	private void changeHarmony() {
		if(generator.getBoolean()) {
			ORCHESTRA.info("███████  change harmony and instrument! ");
			killAllNotes();
			changeInstrument();
		}else {
			changeNote(generator.getInt(MAX_POLYPHONY));
			if(generator.getBoolean()) {
				ORCHESTRA.info("███████  change harmony second note! ");
				changeNote(generator.getInt(MAX_POLYPHONY));
			} 
		}
	}

	private void changeInstrument() {
		changeInstrument(orchestra[generator.getInt(orchestra.length)]);
		setDrony();
	}
	
	
	@Override
	protected long playIteration(final long timePlayed, final long timeToPlay) {
		if(!pausedNotes.isEmpty()) {
			if(isShouldPlay()) {
				playingNotes.addAll(pausedNotes);
				playingNotes.forEach(n -> sendNoteOn(n, VELOCITY));
				
				pausedNotes.clear();
			}else {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
				return timeToPlay+100;
			}
		}else  if(playingNotes.isEmpty()) {
			playNotes();
		}else if(generator.getFrequentBoolean(1)) {
			changeHarmony();
		}
		
		ORCHESTRA.info("████ ████ ████ ████ITERATION");
		printPolyphony();
		int iterationTimePlayed = LENGTH[generator.getInt()%LENGTH.length];
		modulateFor(iterationTimePlayed);
		return timePlayed+iterationTimePlayed;
	}
	
	private void linearCc(int cc, boolean ascending, long iterationTimePlayed) {
		if(ascending){
			ORCHESTRA.info("█                /█");
			ORCHESTRA.info("█               / █");
			ORCHESTRA.info("█              /  █");
			ORCHESTRA.info("█             /   █");
			ORCHESTRA.info("█            /    █");
			ORCHESTRA.info("█           /     █");
			ORCHESTRA.info("█          /      █");
			ORCHESTRA.info("█         /       █");
			ORCHESTRA.info("█        /        █");
			ORCHESTRA.info("█       /         █");
			ORCHESTRA.info("█      /          █");
			ORCHESTRA.info("█     /           █");
			ORCHESTRA.info("█    /            █");
			ORCHESTRA.info("█   /             █");
			ORCHESTRA.info("█  /              █");
			ORCHESTRA.info("█ /               █");
			ORCHESTRA.info("█/                █");
		} else {
			ORCHESTRA.info("█\\                █");
			ORCHESTRA.info("█ \\               █");
			ORCHESTRA.info("█  \\              █");
			ORCHESTRA.info("█   \\             █");
			ORCHESTRA.info("█    \\            █");
			ORCHESTRA.info("█     \\           █");
			ORCHESTRA.info("█      \\          █");
			ORCHESTRA.info("█       \\         █");
			ORCHESTRA.info("█        \\        █");
			ORCHESTRA.info("█         \\       █");
			ORCHESTRA.info("█          \\      █");
			ORCHESTRA.info("█           \\     █");
			ORCHESTRA.info("█            \\    █");
			ORCHESTRA.info("█             \\   █");
			ORCHESTRA.info("█              \\  █");
			ORCHESTRA.info("█               \\ █");
			ORCHESTRA.info("█                \\█");
		}
		int nbIter = (int)iterationTimePlayed/127;
		partialLinear(cc, ascending, nbIter, ascending?0:127, iterationTimePlayed/nbIter);
	}
	private void shCc(int cc, long iterationTimePlayed) {
		ORCHESTRA.info("█                             █");
		ORCHESTRA.info("█                             █");
		ORCHESTRA.info("█                             █");
		ORCHESTRA.info("█          ____               █");
		ORCHESTRA.info("█         |    |              █");
		ORCHESTRA.info("█         |    |              █");
		ORCHESTRA.info("█    __   |    |              █");
		ORCHESTRA.info("█ __|  |  |    |          ___ █");
		ORCHESTRA.info("█|     |__|    |         |   |█");
		ORCHESTRA.info("█|             |         |   |█");
		ORCHESTRA.info("█              |         |   -█");
		ORCHESTRA.info("█              |    _    |    █");
		ORCHESTRA.info("█              |   | |   |    █");
		ORCHESTRA.info("█              |___| |   |    █");
		ORCHESTRA.info("█                    |   |    █");
		ORCHESTRA.info("█                    |___|    █");
		int val;
		int nbIter = (int)iterationTimePlayed/127;
		for(int i=0; i<127;i++) {
			val = generator.getInt(128);
			sendCc(cc, val);
			try {
				Thread.sleep(iterationTimePlayed/nbIter);
			} catch (InterruptedException e) {
			}
		}
	}
	
	private void partialLinear(int cc, boolean ascending, int nbIter, int initVal, long sleepTime){
		int val = initVal;
		for(int i=0; i<nbIter;i++) {
			val = ascending ? (val+1) : val-1;
			val = val <=127 ? val : 127;
			val = val >=0 ? val : 0;
			sendCc(cc, val);
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}
	
	private void triangleCc(int cc, boolean ascending, long iterationTimePlayed) {
		// move will be  (if descending)
		//       descending         ascending         
		// t/4:  63 --> 0            63 --> 127
		// t/2:   0 --> 127         127 --> 0
		// t/4: 127 --> 63            0 --> 64
		int totalCcValues = 127*2;
		long timeSlept = iterationTimePlayed/totalCcValues;
		if(ascending){
			ORCHESTRA.info("█       /\\                       █");
			ORCHESTRA.info("█      /  \\                      █");
			ORCHESTRA.info("█     /    \\                     █");
			ORCHESTRA.info("█    /      \\                    █");
			ORCHESTRA.info("█   /        \\                   █");
			ORCHESTRA.info("█  /          \\                  █");
			ORCHESTRA.info("█ /            \\                 █");
			ORCHESTRA.info("█/              \\                █");
			ORCHESTRA.info("█                \\              /█");
			ORCHESTRA.info("█                 \\            / █");
			ORCHESTRA.info("█                  \\          /  █");
			ORCHESTRA.info("█                   \\        /   █");
			ORCHESTRA.info("█                    \\      /    █");
			ORCHESTRA.info("█                     \\    /     █");
			ORCHESTRA.info("█                      \\  /      █");
			ORCHESTRA.info("█                       \\/       █");
		} else {
			ORCHESTRA.info("█                       /\\       █");
			ORCHESTRA.info("█                      /  \\      █");
			ORCHESTRA.info("█                     /    \\     █");
			ORCHESTRA.info("█                    /      \\    █");
			ORCHESTRA.info("█                   /        \\   █");
			ORCHESTRA.info("█                  /          \\  █");
			ORCHESTRA.info("█                 /            \\ █");
			ORCHESTRA.info("█                /              \\█");
			ORCHESTRA.info("█\\              /                █");
			ORCHESTRA.info("█ \\            /                 █");
			ORCHESTRA.info("█  \\          /                  █");
			ORCHESTRA.info("█   \\        /                   █");
			ORCHESTRA.info("█    \\      /                    █");
			ORCHESTRA.info("█     \\    /                     █");
			ORCHESTRA.info("█      \\  /                      █");
			ORCHESTRA.info("█       \\/                       █");
		}
		partialLinear(cc, ascending, 63, 63, timeSlept);
		partialLinear(cc, !ascending, 127, ascending ? 127 : 0, timeSlept);
		partialLinear(cc, ascending, 64, ascending ? 0 : 127, timeSlept);
	}

	
	private void modulateFor(long iterationTimePlayed) {
		long start = System.currentTimeMillis();
		
//		switch(generator.getInt(24)) {
//		case 0:
//		case 1:
//			// Filter up or down
//			sendCc(SKULPT_CC_FILTER_RESONNANCE, 80);
//			triangleCc(SKULPT_CC_FILTER_CUTOFF, generator.getBoolean(), iterationTimePlayed);
//			break;
//		case 2:
//		case 3:
//			// Filter up or down
//			sendCc(SKULPT_CC_FILTER_RESONNANCE, 127);
//			shCc(SKULPT_CC_FILTER_CUTOFF, iterationTimePlayed);
//			break;
//		case 4:
//		case 5:
//		case 6:
//		case 7:
//			// Modulation up or down
//			// TODO 
//			sendCc(SKULPT_CC_MOD1_SRC, SKULPT_CC_SRC_VAL_LFO1);
//			sendCc(SKULPT_CC_MOD1_DEST, SKULPT_CC_DEST_VAL_CUTOFF);
//			sendCc(SKULPT_CC_MOD1_DEPTH, 127);
//			triangleCc(SKULPT_CC_LFO1_DEPTH, generator.getBoolean(), iterationTimePlayed);
//			break;
//		case 8:
//		case 9:
//		case 10:
//		case 11:
//		case 12:
//		case 13:
//		case 14:
//			sendCc(SKULPT_CC_FILTER_RESONNANCE, 127);
//			triangleCc(SKULPT_CC_FILTER_CUTOFF, true, iterationTimePlayed);
//			break;
//		case 15:
//		case 16:
//		case 18:
//		case 19:
//			// Filter up or down
//			sendCc(SKULPT_CC_FILTER_RESONNANCE, 24);
//			shCc(SKULPT_CC_FILTER_CUTOFF, iterationTimePlayed);
//			break;
//		case 20:
//		case 21:
//		case 22:
//		case 23:
//			try {
//				Thread.sleep(iterationTimePlayed);
//			} catch (InterruptedException e) {
//				throw new RuntimeException(e);
//			}
//			break;
//		}
		
		sendCc(SKULPT_CC_LFO1_RATE, 64);
		sendCc(SKULPT_CC_MOD1_SRC, SKULPT_CC_SRC_VAL_LFO1);
		sendCc(SKULPT_CC_MOD1_DEST, SKULPT_CC_DEST_VAL_CUTOFF);
		sendCc(SKULPT_CC_MOD1_DEPTH, 0);
		triangleCc(SKULPT_CC_LFO1_DEPTH, generator.getBoolean(), iterationTimePlayed);

		long total = System.currentTimeMillis() - start;
		ORCHESTRA.info("██████████ >>>>>>>> "+total+" / "+iterationTimePlayed);
	}

	private int nbNotesPlaying(){
		synchronized (playingNotes) {
			return playingNotes.size();
		}
	}
	
	@Override
	public void hardReset() {
		killAllNotesForce();
	}
	
	private void killAllNotes() {
		synchronized (playingNotes) {
			playingNotes.forEach( note -> sendNoteOff(note));
			playingNotes.clear();
		}
	}
	
	private void killAllNotesForce() {
		for(int nb=0; nb<3; nb++) {
			for(int i=A0; i<=D8_FLAT_FLAT;i++) {
				sendNoteOff(i);
			}
		}
		synchronized (playingNotes) {
			playingNotes.clear();
		}
	}
	
	private void waitForNotesToEnd() {
		ORCHESTRA.info("Waiting for notes to end...");
		long start = System.currentTimeMillis();
		while(nbNotesPlaying()>currentSeries.size()){
			if(System.currentTimeMillis() > (start+MAX_WAIT)) {
				ORCHESTRA.info("█████████████████████████████████████████████████████████████████████████████████████████████████████████");
				ORCHESTRA.info("PANIC!... stop waiting forever");
				ORCHESTRA.info("█████████████████████████████████████████████████████████████████████████████████████████████████████████");
				break;
			}
			try {
				// kill stop last note
				printPolyphony();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		killAllNotes();
	}
}
