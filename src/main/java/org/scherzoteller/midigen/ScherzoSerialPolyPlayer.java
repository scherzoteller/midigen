package org.scherzoteller.midigen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.midiengine.MidiNotes;
import org.scherzoteller.midigen.midiengine.ScherzoMidiDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Serie{
	protected static final Logger ORCHESTRA = LoggerFactory.getLogger("ORCHESTRA");
	protected static final Logger LOGGER = LoggerFactory.getLogger(ScherzoMidiDevice.class);
	private static final int[] BASE_SERIE = new int[]{21,22,23,24,25,26,27,28,29,30,31,32};
	private final int[] serie;
	
	private static int[] copyArray(int[] array) {
		int[] copy = new int[array.length];
		System.arraycopy(array, 0, copy, 0, array.length);
		return copy;
	}
	
	private static int[] shuffleArrayToCopy(Generator generator, int[] array) {
		int[] copy = copyArray(array);
		int index;
		for (int i = copy.length - 1; i > 0; i--) {
			index = generator.getInt(i + 1);
			if (index != i) {
				copy[index] ^= copy[i];
				copy[i] ^= copy[index];
				copy[index] ^= copy[i];
			}
		}
		return copy;
	}
	
	/**
	 * 
	 */
	public Serie(Generator generator){
		this.serie = shuffleArrayToCopy(generator, BASE_SERIE);
	}

	/**
	 * 
	 * @param serie a collection of ser
	 */
	public Serie(int[] serie) {
		this.serie = serie;
	}

	public int[] getSerie() {
		return serie;
	}

	// Unary functions
	public int transpose(int i, int nbOctaves){
		return i+(nbOctaves*12);
	}
	
	public int mirror(int i){
		final int base = MidiNotes.A0; // Since hardcoded, miror should always be the first operation
		return base +  (MidiNotes.NB_NOTES_PER_OCTAVE - 1 - (i - base));
	}

	// Simple Serie functions 
	public Serie transpose(int nbOctaves){
		if(nbOctaves > 7) throw new IllegalArgumentException();
		return new Serie(Arrays.stream(serie).map(i -> transpose(i, nbOctaves)).toArray());
	}

	@Deprecated
	public Serie reverse(){
		// don't use non transposing functions to avoid to low register
		int[] copy = copyArray(serie);
		ArrayUtils.reverse(copy);
		return new Serie(copy);
	}
	
	@Deprecated
	public Serie mirror(){
		// don't use non transposing functions to avoid to low register
		int[] copy = copyArray(serie);
		for(int i=0;i<copy.length;i++){
			copy[i] = mirror(serie[i]);
		}
		return new Serie(copy);
	}

	// Utility 
	public Serie reverseAnd(Function<Integer,Integer> transfo){
		// [22, 28, 27, 31, 24, 30, 26, 25, 23, 32, 21, 29]
		int[] copy = copyArray(this.serie);
		for(int i = 0; i < copy.length / 2; i++)
		{
		    int temp = copy[i];
		    copy[i] = transfo.apply(copy[copy.length - i - 1]);
		    copy[copy.length - i - 1] = transfo.apply(temp);
		}
		return new Serie(copy);
	}

	
	// Double Serie functions 
	public Serie transposeAndReverse(int nbOctaves){
		if(nbOctaves > 7) throw new IllegalArgumentException();
		return reverseAnd(i -> transpose(i, nbOctaves));
	}
	@Deprecated
	public Serie MirrorAndReverse(){
		// don't use non transposing functions to avoid to low register
		return reverseAnd(i -> mirror(i));
	}
	public Serie transposeAndMirror(int nbOctaves){
		int[] copy = copyArray(serie);
		for(int i=0;i<copy.length;i++){
			copy[i] = transpose(mirror(serie[i]), nbOctaves);
		}
		return new Serie(copy);
	}
	
	// Triple Serie functions 
	public Serie transposeMirrorAndReverse(int nbOctaves){
		if(nbOctaves > 7) throw new IllegalArgumentException();
		return reverseAnd(i -> transpose(mirror(i), nbOctaves));
	}

}

class RunningSerie{
	protected static final Logger ORCHESTRA = LoggerFactory.getLogger("ORCHESTRA");
	protected static final Logger LOGGER = LoggerFactory.getLogger(ScherzoMidiDevice.class);
	private final Serie serie;
	private int idx = 0;

	public RunningSerie(Serie serie) {
		this.serie = serie;
	}
	
	public RunningSerie(Serie serie, int idx) {
		this.serie = serie;
		this.idx = idx;
	}
	
	public RunningSerie transformSerie(Function<Serie, Serie> transfo){
		return new RunningSerie(transfo.apply(serie), idx);
	}
	
	public synchronized int next(){
		idx = (idx +1)%serie.getSerie().length;
		return current();
	}

	public synchronized int current() {
		int i = serie.getSerie()[idx];
		ORCHESTRA.info("idx "+idx+" i"+i+" => "+MidiNotes.NOTE_NAMES[i]);
		return i;
	}
}


public class ScherzoSerialPolyPlayer extends ScherzoAbstractPlayer<RunningSerie> implements MidiNotes {
	private static final int MAX_WAIT = 2000;
	private static final boolean WAIT_NOTE_END_BEFORE_SCALE_CHANGE = true;
	private static final int[] LENGTH = {100, 200, 200, 400, 800, 1800}; // , 1800
	private Set<Integer> playingNotes = new TreeSet<>();
	private Serie base;
	private List<RunningSerie> currentSeries = new ArrayList<>(ScherzoMidiDevice.MAX_POLYPHONY);
	private boolean useChords;
	
	public ScherzoSerialPolyPlayer(MidiGenConfig config, AvailableInterfaces iface, int channel, Generator generator) {
		super(config,iface, channel, generator);
	}
	
	public ScherzoSerialPolyPlayer(MidiGenConfig config,AvailableInterfaces iface, int channel, boolean useChords, String instrumentName , int instrument, Generator generator) {
		super(config,iface, channel, generator);
		if(instrument>0){
			changeInstrument(instrument);
		}
		this.useChords = useChords;
		if(this.useChords) {
			LOGGER.debug("chords not supported now");
		}
	}
	
	private void printPolyphony(){
		synchronized(playingNotes){
			StringBuilder sbStars = new StringBuilder();
			for(int i=0;i<playingNotes.size();i++){
				sbStars.append('*');
			}
			StringBuilder sb = new StringBuilder();
			sb.append(StringUtils.leftPad(sbStars.toString(), 10, ' ')).append(' ').append(playingNotes.stream().map(noteIdx -> NOTE_NAMES[noteIdx] ).collect(Collectors.joining(", ")));
			
			if(sb.toString().contains("null")){
				System.err.println("WHAT?");
			}
			
			ORCHESTRA.info(sb.toString());
		}
	}
	
	@Override
	public void playNote(int ignoreNote, int velocity, int durationMs, RunningSerie serie) { // , int[] currentScale, int scaleIdx
		int lastNote = serie.current();
		int note = serie.next();
		if (note > C8) {
			ORCHESTRA.info("Skip note over max, CHORD will be truncated ");
			return;
		}
		synchronized (playingNotes) {
			if (playingNotes.contains(note)) {
				ORCHESTRA.info("Skip duplicated");
				return;
			}
		}
		printPolyphony();
		sendNoteOff(lastNote);
		synchronized (playingNotes) {
			playingNotes.remove(lastNote);
		}
		sendNoteOn(note, velocity);
		synchronized (playingNotes) {
			playingNotes.add(note);
		}
	}
	
	
	@Override
	protected void endPlay() {
		waitForNotesToEnd();
	}
	
	private void displaySerie(String label, Serie s) {
		ORCHESTRA.info("¤¤SERIE "+label+" ¤¤ => "+Arrays.stream(s.getSerie()).mapToObj(i -> NOTE_NAMES[i]).collect(Collectors.joining(",")));
	}
	
	@Override
	protected void initPlay() {
		//F6,A5,G6#/A6b,B5,C6#/D6b,D6,F6#/G6b,C6,G6,D6#/E6b,E6,A5#/B5b
	}

	@Override
	public void reset() {
		this.base = null;
		waitForNotesToEnd();
	}
	
	private void changeSerie() {
		ORCHESTRA.info("███████  change serie! ");
		base = new Serie(generator);
		displaySerie("base", base);
		
		currentSeries.clear();
		//int nbSeries = generator.getInt(ScherzoMidiDevice.MAX_POLYPHONY-1)+2; // no more than ScherzoMidiDevice.MAX_POLYPHONY but at least 1
		int nbSeries = 4;
		for(int i=0; i<nbSeries; i++) {
			Serie derived = derive(base);
			displaySerie("derived", derived);
			currentSeries.add(new RunningSerie(derived));
		}
	}
	
	private Serie derive(Serie original) {
		int nbOctaves = generator.getInt(7);
		switch(generator.getInt(4)) {
		case 0:
			return original.transpose(nbOctaves);
		case 1:
			return original.transposeAndMirror(nbOctaves);
		case 2:
			return original.transposeAndReverse(nbOctaves);
		case 3:
			return original.transposeMirrorAndReverse(nbOctaves);
		}
		return null; // should not happen
	}
	
	
	@Override
	protected long playIteration(final long timePlayed, final long timeToPlay) {
		if(base == null) {
			changeSerie();
		}
		long newTimePlayed = timePlayed + playNote(timePlayed, timeToPlay);
		return newTimePlayed;
	}

	protected long playNote(final long timePlayed, final long timeToPlay) {
		if(generator.getBoolean(2)){
			if(WAIT_NOTE_END_BEFORE_SCALE_CHANGE){
				waitForNotesToEnd();
			}
			//
			changeSerie();
		}
		changeVelocity();
		
		
		int noteLength = LENGTH[generator.getInt()%LENGTH.length];
		
		if(timePlayed + noteLength > timeToPlay){
			noteLength = (int)(timeToPlay - timePlayed);
		}
		
		for(int i=0; i<(generator.getBoolean() ? 1 : 2); i++) {
			int serie = generator.getInt(currentSeries.size());
			ORCHESTRA.info(">>>>>>>>>>>>> something on serie "+serie+" / "+currentSeries.size());
			RunningSerie aSerie = currentSeries.get(serie);
			playNote(-1, currentVelocity, noteLength, aSerie);
		}
		
		try {
			Thread.sleep(noteLength);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		return noteLength;
	}

	private int nbNotesPlaying(){
		synchronized (playingNotes) {
			return playingNotes.size();
		}
	}
	
	@Override
	public void hardReset() {
		this.base = null;
		killAllNotes();
	}
	
	@Override
	public synchronized void pause() {
		super.pause();
		waitForNotesToEnd();
	}
	
	private void killAllNotes() {
		synchronized (playingNotes) {
			playingNotes.forEach( note -> sendNoteOff(note));
			playingNotes.clear();
		}
	}
	
	private void waitForNotesToEnd() {
		ORCHESTRA.info("Waiting for notes to end...");
		long start = System.currentTimeMillis();
		while(nbNotesPlaying()>currentSeries.size()){
			if(System.currentTimeMillis() > (start+MAX_WAIT)) {
				ORCHESTRA.info("█████████████████████████████████████████████████████████████████████████████████████████████████████████");
				ORCHESTRA.info("PANIC!... stop waiting forever");
				ORCHESTRA.info("█████████████████████████████████████████████████████████████████████████████████████████████████████████");
				break;
			}
			try {
				// kill stop last note
				printPolyphony();
				Thread.sleep(100);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		killAllNotes();
	}
	
}
