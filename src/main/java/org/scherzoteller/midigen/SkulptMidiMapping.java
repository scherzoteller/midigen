package org.scherzoteller.midigen;

/*
 * |----+--------------+------+------+--------+------+------+------+------+----|
 * |    |              | 0    | 1    | 2      | 3    | 4    |    5 |    6 |  7 |
 * |----+--------------+------+------+--------+------+------+------+------+----|
 * |    | Parameter    | LFO1 | LFO2 | MOD-EG | NOTE | VELO | AFTT | MODW | CC |
 * |----+--------------+------+------+--------+------+------+------+------+----|
 * |  0 | Wave 1       |      |      |        |      |      |      |      |    |
 * |  1 | Wave 2       |      |      |        |      |      |      |      |    |
 * |  2 | Mix          |      |      |        |      |      |      |      |    |
 * |  3 | Global Pitch |      |      |        |      |      |      |      |    |
 * |  4 | Osc1 Pitch   |      |      |        |      |      |      |      |    |
 * |  5 | Osc2 Pitch   |      |      |        |      |      |      |      |    |
 * |  6 | FM           |      |      |        |      |      |      |      |    |
 * |  7 | LFO1 Rate    |      | X    | X      | X    | X    |      |      |    |
 * |  8 | LFO2 Rate    |      |      |        |      |      |      |      |    |
 * |  9 | LFO1 Shape   | X    | X    | X      | X    | X    |      |      |    |
 * | 10 | LFO2 Shape   |      | X    |        |      |      |      |      |    |
 * | 11 | LFO1 Depth   | X    | X    | X      | X    | X    |      |      |    |
 * | 12 | LFO2 Depth   |      | X    |        |      |      |      |      |    |
 * | 13 | Cutoff       |      |      | X      |      |      |      |      |    |
 * | 14 | Reso         |      |      |        |      |      |      |      |    |
 * | 15 | Morph        |      |      |        |      |      |      |      |    |
 * | 16 | FEG Amount   |      |      | X      |      |      |      |      |    |
 * | 17 | FEG Attack   |      |      | X      |      |      |      |      |    |
 * | 18 | FEG Decay    |      |      | X      |      |      |      |      |    |
 * | 19 | FEG Sustain  |      |      | X      |      |      |      |      |    |
 * | 20 | FEG Release  |      |      | X      |      |      |      |      |    |
 * | 21 | AEG Amount   |      |      | X      |      |      |      |      |    |
 * | 22 | AEG Attack   |      |      | X      |      |      |      |      |    |
 * | 23 | AEG Decay    |      |      | X      |      |      |      |      |    |
 * | 24 | AEG Sustain  |      |      | X      |      |      |      |      |    |
 * | 25 | AEG Release  |      |      | X      |      |      |      |      |    |
 * | 26 | MEG Amount   |      |      | X      |      |      |      |      |    |
 * | 27 | MEG Attack   |      |      | X      |      |      |      |      |    |
 * | 28 | MEG Decay    |      |      | X      |      |      |      |      |    |
 * | 29 | MEG Sustain  |      |      | X      |      |      |      |      |    |
 * | 30 | MEG Release  |      |      | X      |      |      |      |      |    |
 * | 31 | Distortion   |      |      |        |      |      |      |      |    |
 * | 32 | Delay        |      | X    | X      | X    | X    |      |      |    |
 * | 33 | Time         |      | X    | X      | X    | X    |      |      |    |
 * | 34 | Feedback     |      | X    | X      | X    | X    |      |      |    |
 * | 35 | Spread       |      |      |        |      |      |      |      |    |
 * | 36 | Glide        |      |      | X      |      |      |      |      |    |
 * |----+--------------+------+------+--------+------+------+------+------+----|
 */


public interface SkulptMidiMapping {
	int SKULPT_CC_LFO1_RATE = 36;
	int SKULPT_CC_LFO1_DEPTH = 37;
	int SKULPT_CC_FILTER_CUTOFF = 34;
	int SKULPT_CC_FILTER_RESONNANCE = 35;
	int SKULPT_CC_AEG_RELEASE = 29;
	int SKULPT_CC_AEG_ATTACK = 26;
	
	int SKULPT_CC_MOD1_DEPTH = 88;
	int SKULPT_CC_MOD2_DEPTH = 89;
	int SKULPT_CC_MOD3_DEPTH = 90;
	int SKULPT_CC_MOD4_DEPTH = 91;
	int SKULPT_CC_MOD5_DEPTH = 92;
	int SKULPT_CC_MOD6_DEPTH = 93;
	int SKULPT_CC_MOD7_DEPTH = 94;
	int SKULPT_CC_MOD8_DEPTH = 95;
	int SKULPT_CC_MODW_DEPTH = 96;
	int SKULPT_CC_MOD1_SRC = 101;
	int SKULPT_CC_MOD2_SRC = 102;
	int SKULPT_CC_MOD3_SRC = 103;
	int SKULPT_CC_MOD4_SRC = 104;
	int SKULPT_CC_MOD5_SRC = 105;
	int SKULPT_CC_MOD6_SRC = 106;
	int SKULPT_CC_MOD7_SRC = 107;
	int SKULPT_CC_MOD8_SRC = 108;
	int SKULPT_CC_MOD1_DEST = SKULPT_CC_MOD1_SRC;
	int SKULPT_CC_MOD2_DEST = SKULPT_CC_MOD2_SRC;
	int SKULPT_CC_MOD3_DEST = SKULPT_CC_MOD3_SRC;
	int SKULPT_CC_MOD4_DEST = SKULPT_CC_MOD4_SRC;
	int SKULPT_CC_MOD5_DEST = SKULPT_CC_MOD5_SRC;
	int SKULPT_CC_MOD6_DEST = SKULPT_CC_MOD6_SRC;
	int SKULPT_CC_MOD7_DEST = SKULPT_CC_MOD7_SRC;
	int SKULPT_CC_MOD8_DEST = SKULPT_CC_MOD8_SRC;
	
	int SKULPT_CC_SRC_VAL_LFO1 = 0;
	int SKULPT_CC_SRC_VAL_LFO2 = 1;
	int SKULPT_CC_SRC_VAL_MODEG = 2;
	int SKULPT_CC_SRC_VAL_NOTE = 3;
	int SKULPT_CC_SRC_VAL_VELO = 4;
	int SKULPT_CC_SRC_VAL_AFTT = 5;
	int SKULPT_CC_SRC_VAL_MODW = 6;
	int SKULPT_CC_SRC_VAL_CC = 7;
	
	int SKULPT_CC_DEST_VAL_WAVE_1 = 0;
	int SKULPT_CC_DEST_VAL_WAVE_2 = 1;
	int SKULPT_CC_DEST_VAL_MIX = 2;
	int SKULPT_CC_DEST_VAL_GLOBAL_PITCH = 3;
	int SKULPT_CC_DEST_VAL_OSC1_PITCH = 4;
	int SKULPT_CC_DEST_VAL_OSC2_PITCH = 5;
	int SKULPT_CC_DEST_VAL_FM = 6;
	int SKULPT_CC_DEST_VAL_LFO1_RATE = 7;
	int SKULPT_CC_DEST_VAL_LFO2_RATE = 8;
	int SKULPT_CC_DEST_VAL_LFO1_SHAPE = 9;
	int SKULPT_CC_DEST_VAL_LFO2_SHAPE = 10;
	int SKULPT_CC_DEST_VAL_LFO1_DEPTH = 11;
	int SKULPT_CC_DEST_VAL_LFO2_DEPTH = 12;
	int SKULPT_CC_DEST_VAL_CUTOFF = 13;
	int SKULPT_CC_DEST_VAL_RESO = 14;
	int SKULPT_CC_DEST_VAL_MORPH = 15;
	int SKULPT_CC_DEST_VAL_FEG_AMOUNT = 16;
	int SKULPT_CC_DEST_VAL_FEG_ATTACK = 17;
	int SKULPT_CC_DEST_VAL_FEG_DECAY = 18;
	int SKULPT_CC_DEST_VAL_FEG_SUSTAIN = 19;
	int SKULPT_CC_DEST_VAL_FEG_RELEASE = 20;
	int SKULPT_CC_DEST_VAL_AEG_AMOUNT = 21;
	int SKULPT_CC_DEST_VAL_AEG_ATTACK = 22;
	int SKULPT_CC_DEST_VAL_AEG_DECAY = 23;
	int SKULPT_CC_DEST_VAL_AEG_SUSTAIN = 24;
	int SKULPT_CC_DEST_VAL_AEG_RELEASE = 25;
	int SKULPT_CC_DEST_VAL_MEG_AMOUNT = 26;
	int SKULPT_CC_DEST_VAL_MEG_ATTACK = 27;
	int SKULPT_CC_DEST_VAL_MEG_DECAY = 28;
	int SKULPT_CC_DEST_VAL_MEG_SUSTAIN = 29;
	int SKULPT_CC_DEST_VAL_MEG_RELEASE = 30;
	int SKULPT_CC_DEST_VAL_DISTORTION = 31;
	int SKULPT_CC_DEST_VAL_DELAY = 32;
	int SKULPT_CC_DEST_VAL_TIME = 33;
	int SKULPT_CC_DEST_VAL_FEEDBACK = 34;
	int SKULPT_CC_DEST_VAL_SPREAD = 35;
	int SKULPT_CC_DEST_VAL_GLIDE = 36;

}
