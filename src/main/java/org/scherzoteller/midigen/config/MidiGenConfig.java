package org.scherzoteller.midigen.config;

import java.util.Map;

import org.scherzoteller.midigen.gui.MidiGenGUI.SelectedDevice;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;

public class MidiGenConfig {
	private SelectedDevice midiIn;
	private SelectedDevice midiOut;
	private SelectedDevice midiInSkulpt;
	private SelectedDevice midiOutSkulpt;
	private boolean hasPiano;
	private boolean hasPercussions;
	private boolean hasSoftSynth;
	private boolean hasLeadSynth;
	private boolean hasSerial;
	private boolean hasDrone;
	private boolean hasCelesta;
	private boolean hasGervillDrone;
	private boolean hasSkulptBeat;
	@SuppressWarnings("rawtypes")
	private Map snapshots;
	public SelectedDevice getMidiIn() {
		return midiIn;
	}
	public void setMidiIn(SelectedDevice midiIn) {
		this.midiIn = midiIn;
	}
	public SelectedDevice getMidiOut() {
		return midiOut;
	}
	public void setMidiOut(SelectedDevice midiOut) {
		this.midiOut = midiOut;
	}
	
	public SelectedDevice getMidiInSkulpt() {
		return midiInSkulpt;
	}
	public void setMidiInSkulpt(SelectedDevice midiInSkulpt) {
		this.midiInSkulpt = midiInSkulpt;
	}
	public SelectedDevice getMidiOutSkulpt() {
		return midiOutSkulpt;
	}
	public void setMidiOutSkulpt(SelectedDevice midiOutSkulpt) {
		this.midiOutSkulpt = midiOutSkulpt;
	}
	public boolean isHasPiano() {
		return hasPiano;
	}
	public void setHasPiano(boolean hasPiano) {
		this.hasPiano = hasPiano;
	}
	public boolean isHasPercussions() {
		return hasPercussions;
	}
	public void setHasPercussions(boolean hasPercussions) {
		this.hasPercussions = hasPercussions;
	}
	public boolean isHasSoftSynth() {
		return hasSoftSynth;
	}
	public void setHasSoftSynth(boolean hasSoftSynth) {
		this.hasSoftSynth = hasSoftSynth;
	}
	public boolean isHasLeadSynth() {
		return hasLeadSynth;
	}
	public void setHasLeadSynth(boolean hasLeadSynth) {
		this.hasLeadSynth = hasLeadSynth;
	}
	
	public boolean isHasSerial() {
		return hasSerial;
	}
	public void setHasSerial(boolean hasSerial) {
		this.hasSerial = hasSerial;
	}
	
	public boolean isHasDrone() {
		return hasDrone;
	}
	public void setHasDrone(boolean hasDrone) {
		this.hasDrone = hasDrone;
	}
	
	public boolean isHasCelesta() {
		return hasCelesta;
	}
	public void setHasCelesta(boolean hasCelesta) {
		this.hasCelesta = hasCelesta;
	}
	public boolean isHasGervillDrone() {
		return hasGervillDrone;
	}
	public void setHasGervillDrone(boolean hasGervillDrone) {
		this.hasGervillDrone = hasGervillDrone;
	}
	public boolean isHasSkulptBeat() {
		return hasSkulptBeat;
	}
	public void setHasSkulptBeat(boolean hasSkulptBeat) {
		this.hasSkulptBeat = hasSkulptBeat;
	}
	@SuppressWarnings("rawtypes")
	public Map getSnapshots() {
		return snapshots;
	}
	@SuppressWarnings("rawtypes")
	public void setSnapshots(Map snapshots) {
		this.snapshots = snapshots;
	}
	
	
	public SelectedDevice getMidiIn(AvailableInterfaces iface){
		switch(iface){
		case GERVILL:
			return midiIn;
		case SKULPT:
			return midiInSkulpt;
		}
		throw new IllegalStateException("cannot happen");
	}
	
	public SelectedDevice getMidiOut(AvailableInterfaces iface){
		switch(iface){
		case GERVILL:
			return midiOut;
		case SKULPT:
			return midiOutSkulpt;
		}
		throw new IllegalStateException("cannot happen");
	}
	
}
