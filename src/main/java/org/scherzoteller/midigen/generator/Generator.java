package org.scherzoteller.midigen.generator;

public interface Generator {
	int getInt();
	
	long getLong();
	
	default int getInt(int max) {
		return getInt()%max;
	}

	default long getLong(long max) {
		return getLong()%max;
	}


	boolean getBoolean();
	
	default boolean getBoolean(int oneChanceOn){
		boolean res = true;
		for(int i=0; i<oneChanceOn;i++){
			res &=getBoolean();
		}
		return res;
	}
	boolean getFrequentBoolean(int oneChanceOn);

}
