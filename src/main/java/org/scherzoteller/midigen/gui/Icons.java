package org.scherzoteller.midigen.gui;

import javax.swing.ImageIcon;

public enum Icons {
	PLAY("/icons/play.png"), PAUSE("/icons/pause.png"), STOP("/icons/stop.png");
	private final String url;
	private Icons(String url) {
		this.url = url;
	}
	
	public ImageIcon getImage() {
		return new ImageIcon(Icons.class.getResource(url));
	}
}
