package org.scherzoteller.midigen.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.scherzoteller.midigen.MidiGen;
import org.scherzoteller.midigen.ScherzoClassicMonoPlayer;
import org.scherzoteller.midigen.ScherzoSerialPolyPlayer;
import org.scherzoteller.midigen.config.MidiGenConfig;
import org.scherzoteller.midigen.generator.Generator;
import org.scherzoteller.midigen.midiengine.AvailableInterfaces;
import org.scherzoteller.midigen.play.CompositePlayer;
import org.scherzoteller.midigen.processing.ProcessingApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MidiGenGUI extends JFrame implements Generator {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MidiGenGUI.class);
	private static final Logger ORCHESTRA = LoggerFactory.getLogger("ORCHESTRA");
	private static final int LONG_SIZE = String.valueOf(Long.MAX_VALUE).length() - 1;

	private CompositePlayer player;
	private static final String TITLE = "Turing Machine";
	private static final String TXTA_TITLE = "Turing Machine";
	private JTextArea jTextArea;
	private Long ax;
	private Long bx;
	private Long cx;
	private Long dx;
	private Long ix;
	private Long jx;
	private Long kx;
	private Long lx;
	private JTextField axField;
	private JTextField bxField;
	private JTextField cxField;
	private JTextField dxField;
	private JTextField ixField;
	private JTextField jxField;
	private JTextField kxField;
	private JTextField lxField;
	private JTextField lastIntField;
	private JTextField lastLongField;
	private JTextField lastBooleanField;
	private JPanel registerPanel;
	private JComboBox<String> snapshots;
	private String number = ""; // THE NUMBER!
	
	private int mpX, mpY;

	
	
	
	public MidiGenGUI() {
		super(TITLE);
		this.setLayout(new BorderLayout());
		this.setBackground(Color.DARK_GRAY);
		JLabel titleLabel = new JLabel(TXTA_TITLE);
		titleLabel.setOpaque(true);
		titleLabel.setBackground(Color.BLACK);
		titleLabel.setForeground(Color.WHITE);
		setUndecorated(true);
		titleLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if((e.getModifiers() & MouseEvent.CTRL_MASK) != 0 && (e.getModifiers() & MouseEvent.ALT_MASK) != 0 && (e.getModifiers() & MouseEvent.SHIFT_MASK) != 0){
					System.exit(0);
				}
				mpX = e.getX();
				mpY = e.getY();
			}
		});
		
		titleLabel.addMouseMotionListener( new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				setLocation(
			              getLocation().x + e.getX() - mpX,
			              getLocation().y + e.getY() - mpY );
			}
		});
		
		
		
		this.add(titleLabel, BorderLayout.NORTH);
		this.jTextArea = new JTextArea(10, 30);
		this.add(jTextArea, BorderLayout.CENTER);
		this.registerPanel = new JPanel(new GridLayout(6, 4));
		this.add(registerPanel, BorderLayout.EAST);
		initRegisters();
		this.axField = getRegister(registerPanel, "REGA");
		this.bxField = getRegister(registerPanel, "REGB");
		this.cxField = getRegister(registerPanel, "REGC");
		this.dxField = getRegister(registerPanel, "REGD");
		this.ixField = getRegister(registerPanel, "REGI");
		this.jxField = getRegister(registerPanel, "REGJ");
		this.kxField = getRegister(registerPanel, "REGK");
		this.lxField = getRegister(registerPanel, "REGL");
		refreshRegisters();
		
		
		manageSnapshots();
		JButton resetButton = addButton("Reset", e -> this.resetRegisters());
		
		
		registerPanel.add(resetButton);
		// 
		{
			JLabel popLabel = new JLabel("POP");
			registerPanel.add(popLabel);
			JPanel testButtonsPanel = new JPanel(new GridLayout(1, 3));
			testButtonsPanel.add(addButton("Int", e -> this.getInt()));
			testButtonsPanel.add(addButton("Long", e -> this.getLong()));
			testButtonsPanel.add(addButton("Bool", e -> this.getBoolean()));
			registerPanel.add(testButtonsPanel);
		}
		
		
		this.lastLongField = getRegister(registerPanel, "Last Long");
		this.lastIntField = getRegister(registerPanel, "Last Int");
		this.lastBooleanField = getRegister(registerPanel, "Last Bool");
		jTextArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				keyWasPressed();
			}
		});
		
		// Change colors

		setEntryColor(jTextArea, axField, bxField, cxField, dxField, ixField, jxField, kxField, lxField, lastLongField, lastIntField, lastBooleanField);
		setPanelColor(this, registerPanel);
		setButtonColor(resetButton);
		this.pack();
		this.repaint();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void manageSnapshots() {
		if(MidiGen.getConfig() == null || MidiGen.getConfig().getSnapshots() == null){
			Stream.generate(() -> new JLabel("no snapshots") ).limit(5).forEach(registerPanel::add); // Add 5 blank jlabels to keep consistent UI
			return;
		}
		snapshots = new JComboBox<String>((String[])MidiGen.getConfig().getSnapshots().keySet().toArray(new String[MidiGen.getConfig().getSnapshots().keySet().size()]));
		registerPanel.add(snapshots);
		JButton snapShotChange = addButton("Load snapShot", e -> {
			Map<String, String> object = (Map)MidiGen.getConfig().getSnapshots().get(snapshots.getSelectedItem());
			ax = Long.valueOf(object.get("REGA"));
			bx = Long.valueOf(object.get("REGB"));
			cx = Long.valueOf(object.get("REGC"));
			dx = Long.valueOf(object.get("REGD"));
			ix = Long.valueOf(object.get("REGI"));
			jx = Long.valueOf(object.get("REGJ"));
			kx = Long.valueOf(object.get("REGK"));
			lx = Long.valueOf(object.get("REGL"));
			refreshRegisters();
			keyWasPressed();
		});
		registerPanel.add(snapShotChange);
		
		JTextField newSnapshotName = new JTextField();
		registerPanel.add(newSnapshotName);
		JButton addSnapshotButton = new JButton("Add snapshot");
		
		registerPanel.add(addSnapshotButton);
		
		addSnapshotButton.addActionListener(ev -> {
			if(MidiGen.getConfig().getSnapshots().containsKey(newSnapshotName.getText())){
				LOGGER.error("Snapshot already used!");
				return;
			}
			HashMap<Object, Object> newSnapshot = new HashMap<>();
			newSnapshot.put("REGA", String.valueOf(ax));
			newSnapshot.put("REGB", String.valueOf(bx));
			newSnapshot.put("REGC", String.valueOf(cx));
			newSnapshot.put("REGD", String.valueOf(dx));
			newSnapshot.put("REGI", String.valueOf(ix));
			newSnapshot.put("REGJ", String.valueOf(jx));
			newSnapshot.put("REGK", String.valueOf(kx));
			newSnapshot.put("REGL", String.valueOf(lx));
			MidiGen.getConfig().getSnapshots().put(newSnapshotName.getText(), newSnapshot);
			snapshots.addItem(newSnapshotName.getText());
			newSnapshotName.setText("");
		});
		
		
		
		JButton saveSnapshotButton = new JButton("Save snapshots");
		registerPanel.add(saveSnapshotButton);
		saveSnapshotButton.addActionListener(ev -> {
			ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(MidiGen.PERF_FILE, MidiGen.getConfig().getSnapshots());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});
		setEntryColor(newSnapshotName, snapshots);
		
		setButtonColor(snapShotChange, addSnapshotButton, saveSnapshotButton);
	}

	private void setEntryColor(Component... components) {
		Stream.of(components).forEach(c -> {
			c.setBackground(Color.BLACK);
			c.setForeground(Color.CYAN);
		});
	}

	private void setPanelColor(Component... components) {
		Stream.of(components).forEach(c -> {
			c.setBackground(Color.DARK_GRAY);
			c.setForeground(Color.WHITE);
		});
	}

	private void setButtonColor(Component... components) {
		Stream.of(components).forEach(c -> {
			c.setBackground(Color.BLACK);
			c.setForeground(Color.GREEN);
		});
	}
	
	public void resetRegisters() {
		initRegisters();
		refreshRegisters();
		this.jTextArea.setText("");
		this.number = "";
	}
	
	private static long shuffle(long l){
		return Math.abs(l);
//		l = l*31l;
//		int nbRotations = (int)~l&7; 
//		l = Math.abs(l+rotate(l, nbRotations));
//		return l;
	}
	

	 private synchronized void initRegisters() {
		 this.ax = 4546787456548642167l;
		 this.bx = 3940584512309408753l;
		 this.cx = 6687042100551265434l;
		 this.dx = 8705432154989840635l;
		 this.ix = 0l;
		 this.jx = 0l;
		 this.kx = 0l;
		 this.lx = 0l;
	 }

	private JButton addButton(String text, ActionListener action) {
		JButton b = new JButton(text);
		b.addActionListener(action);
		
		b.setBackground(Color.BLACK);
		b.setForeground(Color.GREEN);

		return b;
	}

	private JButton addButton(Icons icon, ActionListener action) {
		JButton b = new JButton(icon.getImage());
		b.addActionListener(action);
		b.setBackground(Color.BLACK);
		b.setForeground(Color.GREEN);
		return b;
	}

	public void addPlayer(CompositePlayer player) {
		this.player = player;
		JPanel playPanel = new JPanel(new GridLayout(1, 3));
		playPanel.add(addButton(Icons.PLAY, e -> player.start()));
		playPanel.add(addButton(Icons.PAUSE, e -> player.pause()));
		playPanel.add(addButton(Icons.STOP, e -> {
			player.pause();
			if((e.getModifiers()&InputEvent.CTRL_MASK) != 0) {
				player.hardReset();
			}else {
				player.reset();
			}
		}));
		this.registerPanel.add(playPanel);
		
		JButton addCelestaBtn = addButton("+Celesta", ev -> {
			String instrumentLabel = "CELESTA_"+System.currentTimeMillis();
			ScherzoClassicMonoPlayer celestaPlayer = new ScherzoClassicMonoPlayer(MidiGen.getConfig(), AvailableInterfaces.GERVILL, MidiGen.assignChannel(instrumentLabel, AvailableInterfaces.GERVILL), 9, this);
			this.player.addPlayer(celestaPlayer, instrumentLabel);
		});
		registerPanel.add(addCelestaBtn);

		JButton addSerialBtn = addButton("+Serial", ev -> {
			String instrumentLabel = "SERIAL_"+System.currentTimeMillis();
			ScherzoSerialPolyPlayer serialPlayer = new ScherzoSerialPolyPlayer(MidiGen.getConfig(), AvailableInterfaces.GERVILL, MidiGen.assignChannel(instrumentLabel, AvailableInterfaces.GERVILL), true, instrumentLabel, 0, this);
			this.player.addPlayer(serialPlayer, instrumentLabel);
		});
		registerPanel.add(addSerialBtn);
		setButtonColor(addCelestaBtn, addSerialBtn);

		this.pack();
		this.repaint();
	}
	
	public static class SelectedDevice{
		private final MidiDevice midiDevice;
		public static final SelectedDevice NONE = new SelectedDevice();
		
		private SelectedDevice() {
			midiDevice = null;
		}
		public SelectedDevice(MidiDevice midiDevice) {
			this.midiDevice = midiDevice;
		}

		public MidiDevice getMidiDevice() {
			return midiDevice;
		}
		
		public boolean isIn() {
			return midiDevice != null && midiDevice.getMaxTransmitters() != 0;
		}
		public boolean isOut() {
			return midiDevice != null && midiDevice.getMaxReceivers() != 0;
		}
		
		@Override
		public String toString() {
			if(midiDevice == null) return "(none)";
			
			StringBuilder sb = new StringBuilder(midiDevice.getDeviceInfo().getName());
			int nbRec = midiDevice.getMaxReceivers();
			if(nbRec != 0) {
				sb.append(" - ").append(nbRec>0 ? nbRec : "all").append(" inputs");
			}
			int nbTrans = midiDevice.getMaxTransmitters();
			if(nbTrans != 0) {
				sb.append(" - ").append(nbTrans>0 ? nbTrans : "all").append(" outputs");
			}
			return sb.toString();
		}
		
	}
	
	private static SelectedDevice findDevice(List<SelectedDevice> list, String prefered, String fallback){
		SelectedDevice preferedDevice = findDevice(list, prefered);
		if(preferedDevice == SelectedDevice.NONE){
			return findDevice(list, fallback);
		}else{
			return preferedDevice;
		}
	}
	
	private static SelectedDevice findDevice(List<SelectedDevice> list, String prefered){
		Optional<SelectedDevice> dev = list.stream().filter( o -> o.getMidiDevice() != null && o.getMidiDevice().getDeviceInfo().getName().contains(prefered)).findAny();
		if(dev.isPresent()){
			return dev.get();
		}else{
			return SelectedDevice.NONE;
		}
	}
	
	
	
	
	
	public static void selectDevice(Consumer<MidiGenConfig> andAfter) {
		// [obtain and open the three devices...]
		Info[] midiDeviceInfo = MidiSystem.getMidiDeviceInfo();
		List<SelectedDevice> labelsIns = new ArrayList<>();
		List<SelectedDevice> labelsOuts = new ArrayList<>();
		labelsIns.add(SelectedDevice.NONE);	
		labelsOuts.add(SelectedDevice.NONE);
		
		try {
			
			final boolean hasSkulpt = true;
//			final boolean hasSkulpt = Stream.of(midiDeviceInfo).anyMatch( info ->  info.getVendor().contains("Skulpt"));
			final MidiGenConfig config = new MidiGenConfig();

			for (int i = 0; i < midiDeviceInfo.length; i++) {
				Info info = midiDeviceInfo[i];
				SelectedDevice dev = new SelectedDevice(MidiSystem.getMidiDevice(info));
				if(dev.isIn()) {
					labelsIns.add(dev);
				}
				if(dev.isOut()) {
					labelsOuts.add(dev);
				}
			}
			JComboBox<SelectedDevice> comboIns = new JComboBox<>(labelsIns.toArray(new SelectedDevice[labelsIns.size()]));
			JComboBox<SelectedDevice> comboOuts = new JComboBox<>(labelsOuts.toArray(new SelectedDevice[labelsOuts.size()]));
			
			// select defaults
			comboIns.setSelectedItem(findDevice(labelsIns, "Sequencer"));
			comboOuts.setSelectedItem(findDevice(labelsOuts, "Gervill"));

			
			String title = "Choose a device";
			JFrame choiceWindow = new JFrame(title);
			choiceWindow.setLayout(new GridLayout(8, 2));
			
			choiceWindow.add(comboIns);
			choiceWindow.add(comboOuts);
			
			
			final ActionListener skulptEv;
			if(hasSkulpt){
				JComboBox<SelectedDevice> comboIns2 = new JComboBox<>(labelsIns.toArray(new SelectedDevice[labelsIns.size()]));
				JComboBox<SelectedDevice> comboOuts2 = new JComboBox<>(labelsOuts.toArray(new SelectedDevice[labelsOuts.size()]));
				// select defaults
				comboIns2.setSelectedItem(findDevice(labelsIns, "Skulpt", "Sequencer"));
				comboOuts2.setSelectedItem(findDevice(labelsOuts, "Skulpt", "Gervill"));
				choiceWindow.add(comboIns2);
				choiceWindow.add(comboOuts2);

				skulptEv = e -> {
					config.setMidiIn((SelectedDevice)comboIns2.getSelectedItem());
					config.setMidiOut((SelectedDevice)comboOuts2.getSelectedItem());
				};
			}else{
				skulptEv = e -> {};
			}
			
			
			JButton button = new JButton("Continue");
			JCheckBox pianoCkb = new JCheckBox("Piano", true);
			choiceWindow.add(pianoCkb);
			JCheckBox percCkb = new JCheckBox("Percussions");
			choiceWindow.add(percCkb);
			JCheckBox softSynthCkb = new JCheckBox("Soft Synth");
			choiceWindow.add(softSynthCkb);
			JCheckBox leadCkb = new JCheckBox("Lead Synth");
			choiceWindow.add(leadCkb);
			
			JCheckBox serialCkb = new JCheckBox("Serial");
			choiceWindow.add(serialCkb);
			JCheckBox droneCkb = new JCheckBox("Skulpt Drone");
			choiceWindow.add(droneCkb);
			JCheckBox celestaCkb = new JCheckBox("Celesta");
			choiceWindow.add(celestaCkb);
			JCheckBox droneGervillCkb = new JCheckBox("Gervill Drone");
			choiceWindow.add(droneGervillCkb);
			JCheckBox skulptPercsCkb = new JCheckBox("Skulpt beat");
			choiceWindow.add(skulptPercsCkb);
			
			choiceWindow.add(new JLabel(""));
			choiceWindow.add(button);
			choiceWindow.pack();
			choiceWindow.setVisible(true);
			
			button.addActionListener(ev -> {
				choiceWindow.setVisible(false);
				config.setMidiIn((SelectedDevice)comboIns.getSelectedItem());
				config.setMidiOut((SelectedDevice)comboOuts.getSelectedItem());
				
				skulptEv.actionPerformed(ev);
				
				config.setHasPiano(pianoCkb.isSelected());
				config.setHasPercussions(percCkb.isSelected());
				config.setHasSoftSynth(softSynthCkb.isSelected());
				config.setHasLeadSynth(leadCkb.isSelected());
				config.setHasSerial(serialCkb.isSelected());
				config.setHasCelesta(celestaCkb.isSelected());
				config.setHasGervillDrone(droneGervillCkb.isSelected());
				if(hasSkulpt){
					config.setHasDrone(droneCkb.isSelected());
					config.setHasSkulptBeat(skulptPercsCkb.isSelected());
				}

				if(MidiGen.PERF_FILE != null) {
					if(MidiGen.PERF_FILE.length() == 0){
						config.setSnapshots(new LinkedHashMap<>());
					}else{
						ObjectMapper mapper = new ObjectMapper();
						try {
							config.setSnapshots(mapper.readValue(MidiGen.PERF_FILE, LinkedHashMap.class));
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
				}
				if(!Stream.of(pianoCkb, percCkb, softSynthCkb, leadCkb, serialCkb, droneCkb, celestaCkb, droneGervillCkb, skulptPercsCkb).anyMatch( c -> c.isSelected())) {
					ORCHESTRA.error("Nothing to play dude");
					System.exit(1);
				}
				if(SelectedDevice.NONE.equals(comboIns.getSelectedItem()) || SelectedDevice.NONE.equals(comboIns.getSelectedItem())) {
					ORCHESTRA.error("Nowhere to play dude");
					System.exit(1);
				}
				
				andAfter.accept(config);
				
				
			});
			
			choiceWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		} catch (MidiUnavailableException e) {
			throw new RuntimeException(e);
		}
	}



	private JTextField getRegister(JPanel registerPanel, String label, String initialValue) {
		JPanel p = new JPanel(new GridLayout(1, 2));
		JLabel jLabel = new JLabel(label);
		p.add(jLabel);
		JTextField jTextField = new JTextField();
		if (initialValue != null) {
			jTextField.setText(initialValue);
		}
		p.add(jTextField);
		registerPanel.add(p);
		setPanelColor(p, jLabel);
		return jTextField;
	}

	private JTextField getRegister(JPanel registerPanel, String label) {
		return getRegister(registerPanel, label, (String) null);
	}

	private synchronized long getLongFromBuffer(String number, int idx, int nbDigits) {
		StringBuilder sb = new StringBuilder();
		if (number.length() - idx < nbDigits) {
			sb.append(number.substring(idx, number.length()));
		} else {
			sb.append(number.substring(idx, idx + nbDigits));

		}
		while (sb.length() < nbDigits) {
			if (number.length() <= (nbDigits - sb.length())) {
				sb.append(number);
			} else {
				sb.append(number.substring(0, nbDigits - sb.length()));
			}
		}
		try {
			return Long.valueOf(sb.toString());
		} catch (NumberFormatException e) {
			LOGGER.error("getLongFromBuffer(" + number + ", " + idx + ", " + nbDigits + ")");
			throw e;
		}
	}

	private synchronized long getLongFromBuffer(int idx, int nbDigits) {
		return getLongFromBuffer(number, idx, nbDigits);

	}

	private synchronized long getLongFromBuffer(int idx) {
		return getLongFromBuffer(idx, LONG_SIZE);
	}

	private synchronized long getLongFromBuffer(long base) {
		return getLongFromBuffer(getInt(base, this.number.length(), false));
	}

	private long defaultIfZero(long val, long fallback) {
		return val == 0 ? fallback : val;
	}

	private void keyWasPressed() {
		String oldNumber = this.number;
		String text = jTextArea.getText();
		this.number = text.replaceAll("[^0-9]+", "");

		if (!oldNumber.equals(this.number)) {
			messageWasChanged();
		}
	}

	private void messageWasChanged() {
		if (this.number.length() >= 3) {
			this.ix = getLongFromBuffer(ax ^ bx);
			this.jx = getLongFromBuffer(rotate(cx, 4) ^ dx);
			this.kx = getLongFromBuffer(dx ^ ax);
			this.lx = getLongFromBuffer(rotate(cx, 9) ^ bx);
		} else {
			this.ix = Math.abs(~ax ^ defaultIfZero(jx, dx));
			this.jx = Math.abs(~bx ^ defaultIfZero(kx, cx));
			this.kx = Math.abs(~cx ^ defaultIfZero(lx, bx));
			this.lx = Math.abs(~dx ^ defaultIfZero(ix, ax));
		}
		iterate();
		refreshRegisters();
	}
	private synchronized void iterate() {
		iterate((int) ((ax ^ bx) % 3) + 1);
	}

	private synchronized void iterate(int nbIteration) {
		if (nbIteration > 0) {
			// Previous was Math.abs instead of shuffle => often converging
			this.ax = shuffle(rotate(ix, dx % 5) ^ dx);
			this.bx = shuffle(jx ^ rotate(cx, bx % 9));
			this.cx = shuffle(rotate(kx, bx % 7) ^ bx);
			this.dx = shuffle(lx ^ rotate(ax, ax % 4));
			iterate(nbIteration - 1);
		}
	}


	private static long rotate(long input, int k) {
		return (input >>> k) | (input << (Long.SIZE - k));
	}

	private static long rotate(long input, long k) {
		return rotate(input, (int) k);
	}

	private long getLong(long base, int max, boolean nonZero) {
		long a = base % max;
		return Math.abs(nonZero ? (a + 1) : a);
	}

	private int getInt(long l) {
		// TODO bit operation
		return Math.abs((int) l);
	}

	private int getInt(long base, int max, boolean nonZero) {
		return getInt(getLong(base, max, nonZero));
	}

	private void refreshRegisters() {
		this.axField.setText(ax.toString());
		this.bxField.setText(bx.toString());
		this.cxField.setText(cx.toString());
		this.dxField.setText(dx.toString());
		this.ixField.setText(ix.toString());
		this.jxField.setText(jx.toString());
		this.kxField.setText(kx.toString());
		this.lxField.setText(lx.toString());
	}

	@Override
	public synchronized int getInt() {
		iterate();
		refreshRegisters();
		int ret = bx % 2 == 0 ? getInt(dx) : getInt(ax);
		String str = String.valueOf(ret);
		lastIntField.setText(str);
		ProcessingApp.addNumber(str);
		
		return ret;
	}

	@Override
	public synchronized long getLong() {
		iterate();
		refreshRegisters();
		Long ret = ax % 2 == 0 ? cx : bx;
		LOGGER.debug("getLong(): ret = " + ret);
		lastLongField.setText(String.valueOf(ret));
		return ret;
	}

	@Override
	public synchronized boolean getBoolean() {
		boolean ret = getBooleanNoState();
		refreshRegisters();
		lastBooleanField.setText(String.valueOf(ret));
		return ret;
	}

	private synchronized boolean getBooleanNoState() {
		iterate();
		return (~bx ^ rotate(cx, ax % 7) ^ dx) % 2 > 0;
	}

	@Override
	public synchronized boolean getBoolean(int oneChanceOn) {
		boolean ret = true;
		for (int i = 0; i < oneChanceOn; i++) {
			ret &= getBooleanNoState();
		}
		lastBooleanField.setText(String.valueOf(ret));
		return ret;
	}

	@Override
	public synchronized boolean getFrequentBoolean(int oneChanceOn) {
		boolean ret = true;
		for (int i = 0; i < oneChanceOn; i++) {
			ret |= getBooleanNoState();
		}
		lastBooleanField.setText(String.valueOf(ret));
		return ret;
	}

}
