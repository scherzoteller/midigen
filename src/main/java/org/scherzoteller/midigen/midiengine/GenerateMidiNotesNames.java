package org.scherzoteller.midigen.midiengine;

public class GenerateMidiNotesNames {
	private static final int NOTE_MAX = 87;
	private static final int NOTE_IDX_OCTAVE_SWITCH = 2;
	private static final int NOTE_OFFSET = 21;
	private static final int NB_NOTES = 7;
	private static final char[] NOTE_NAMES = "ABCDEFG".toCharArray(); // Note names
	private static final boolean[] HAS_SHARP = {true, false, true, true, false, true, true};
	
	private static final void printNote(int i, int noteIdx, int octaveIdx, String suffix){
		System.err.println("int "+NOTE_NAMES[noteIdx]+""+octaveIdx+suffix +" = "+(i+NOTE_OFFSET)+";");
	}
	
	private static final int nextNote(int noteIdx){
		return (noteIdx+1)%NB_NOTES;
	}
	
	private static final int previousNote(int noteIdx){
		return noteIdx == 0 ? NB_NOTES-1 : noteIdx - 1;
	}
	
	public static void main(String[] args) {
		int noteIdx = 0;
		int octaveIdx = 0;
		for(int i=0;i<=NOTE_MAX;i++){
			printNote(i, noteIdx, octaveIdx, "");
			if(HAS_SHARP[previousNote(noteIdx)]){
				printNote(i, previousNote(noteIdx), octaveIdx, "_SHARP_SHARP");
			}
			if(HAS_SHARP[noteIdx] && i<NOTE_MAX){
				++i;
				printNote(i, noteIdx, octaveIdx, "_SHARP");
				printNote(i, nextNote(noteIdx), octaveIdx, "_FLAT");
			}else if(!HAS_SHARP[noteIdx]) {
				printNote(i, nextNote(noteIdx), octaveIdx, "_FLAT");
				if (i<NOTE_MAX){
					printNote(i+1, noteIdx, octaveIdx, "_SHARP");
				}
			}
			if(HAS_SHARP[noteIdx]){
				printNote(i, nextNote(noteIdx), octaveIdx, "_FLAT_FLAT");
			}
			
			noteIdx=(noteIdx+1)%NB_NOTES;
			if(noteIdx == NOTE_IDX_OCTAVE_SWITCH) octaveIdx++;
		}
	}
}
