package org.scherzoteller.midigen.midiengine;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Transmitter;

import org.scherzoteller.midigen.config.MidiGenConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScherzoMidiDevice<D> implements AutoCloseable {
	public static final int MAX_POLYPHONY = 4; // TODO add to GUI
	private Receiver destination;
	private int channel;
	
	private List<MidiDevice> openDevices;
	
	protected static final Logger ORCHESTRA = LoggerFactory.getLogger("ORCHESTRA");
	protected static final Logger LOGGER = LoggerFactory.getLogger(ScherzoMidiDevice.class);

	public static void debug() {
		// [obtain and open the three devices...]
		Info[] midiDeviceInfo = MidiSystem.getMidiDeviceInfo();
		try {
			for (Info mdi : midiDeviceInfo) {
				LOGGER.debug(String.valueOf(mdi));
				MidiDevice midiDevice = MidiSystem.getMidiDevice(mdi);
				LOGGER.debug("MaxReceivers (MIDI IN): " + midiDevice.getMaxReceivers());
				LOGGER.debug("MaxTransmitters (MIDI OUT): " + midiDevice.getMaxTransmitters());
				LOGGER.debug("-------------------------------");
			}
		} catch (MidiUnavailableException e) {
			LOGGER.error(e.toString(), e);
			throw new RuntimeException(e);
		}
	}
	
	private static MidiDevice getDeviceByPredicate(Predicate<Info> p) {
		try {
			return MidiSystem.getMidiDevice(Stream.of(MidiSystem.getMidiDeviceInfo()).filter(p).findFirst().get());
		} catch (MidiUnavailableException e) {
			LOGGER.error(e.toString(), e);
			throw new RuntimeException(e);
		}
	}
	private static MidiDevice getDeviceByPartialName(String partialName) {
		return getDeviceByPredicate(mdi -> mdi.getName().contains(partialName));
	}
	
	

	public ScherzoMidiDevice(String synthDevice, String seqDevice, int channel) {
		this(getDeviceByPartialName(synthDevice),getDeviceByPartialName(seqDevice), channel);
	}
	
	protected void changeInstrument(int instrument){
		changeInstrument(destination, channel, instrument);
	}
	protected void changeInstrument(Receiver receiver, int channel, int instrument){
		ORCHESTRA.info(">>>>>>>>>>>>> Program change for channel "+channel+" on receiver "+receiver.toString()+" => instr "+instrument);
		sendMessage(receiver, ShortMessage.CONTROL_CHANGE, channel, 0,  1152 >> 7);
		sendMessage(receiver, ShortMessage.CONTROL_CHANGE, channel, 32, 1152 & 0x7f);
		sendMessage(receiver, ShortMessage.PROGRAM_CHANGE, channel, instrument, 0);
	}
	
	
	public ScherzoMidiDevice(MidiDevice devSynth, MidiDevice devSeq, int channel) {
		try {
			ORCHESTRA.info("Opening seq "+devSeq+" to synth "+devSynth);
			openDevices = Stream.of(devSeq, devSynth).collect(Collectors.toList());
			devSynth.open();
			devSeq.open();
			
			Transmitter inPortTrans1;
			inPortTrans1 = devSeq.getTransmitter();
			inPortTrans1.setReceiver(devSynth.getReceiver());
			destination = devSynth.getReceiver();
			this.channel = channel;
		} catch (MidiUnavailableException e) {
			LOGGER.error(e.toString(), e);
			throw new RuntimeException(e);
		}
		
	}

	public ScherzoMidiDevice(MidiGenConfig config, AvailableInterfaces iface, int channel) {
		this(config.getMidiOut(iface).getMidiDevice(), config.getMidiIn(iface).getMidiDevice(), channel);
	}
	
	public void sendMessage(Receiver destination, int command, int channel, int data1, int data2) {
		try {
			ShortMessage myMsg = new ShortMessage();
			myMsg.setMessage(command, channel, data1, data2);
			long timeStamp = -1;
			destination.send(myMsg, timeStamp);
		} catch (InvalidMidiDataException e) {
			LOGGER.error(e.toString(), e);
			throw new RuntimeException(e);
		}
	}

	public synchronized void sendMessage(int command, int data1, int data2) {
		sendMessage(destination, command, channel, data1, data2);
	}

	public void sendNoteOn(int note, int velocity) {
		sendMessage(ShortMessage.NOTE_ON, note, velocity);
	}

	public void sendCc(int cc, int val) {
		ORCHESTRA.info("CC "+cc+" => "+val);
		sendMessage(ShortMessage.CONTROL_CHANGE, cc, val);
	}

	public void sendNoteOff(int note) {
		sendMessage(ShortMessage.NOTE_OFF, note, 0);
	}

	public void playNote(int note, int velocity, int durationMs, D data) {
		try {
			sendNoteOn(note, velocity);
			Thread.sleep(durationMs);
			sendNoteOff(note);
		} catch (InterruptedException e) {
			LOGGER.error(e.toString(), e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void close() throws Exception {
		openDevices.forEach(MidiDevice::close);
	}
	
	
}
