package org.scherzoteller.midigen.play;

import java.util.Arrays;
import java.util.Collection;

import org.scherzoteller.midigen.MidiGen;

public class CompositePlayer implements Player {
	private Collection<Player> players;
	private boolean isPlaying = false;
	
	public CompositePlayer(Player...players){
		this(Arrays.asList(players));
	}
	public CompositePlayer(Collection<Player> players){
		this.players = players;
	}
	
	public void addPlayer(Player p, String instrumentLabel) {
		players.add(p);
		
		
		MidiGen.executorService.submit(() -> {
			Thread.currentThread().setName("Dynamic Thread "+instrumentLabel);
			p.play();
		});
		if(isPlaying) {
			p.start();
		}
	}
	
	
	@Override
	public void start() {
		isPlaying = true;
		players.stream().forEach(Player::start);
	}
	
	@Override
	public void pause() {
		isPlaying = false;
		players.stream().forEach(Player::pause);
	}
	
	@Override
	public void reset() {
		players.stream().forEach(Player::reset);
	}

	@Override
	public void hardReset() {
		players.stream().forEach(Player::hardReset);
	}

	@Override
	public void play() {
		players.stream().forEach(Player::play);
	}
}
