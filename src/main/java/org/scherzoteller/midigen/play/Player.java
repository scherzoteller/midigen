package org.scherzoteller.midigen.play;

public interface Player {
	void start();
	void pause();
	void play();
	default void reset(){
	}
	default void hardReset(){
	}
}
