package org.scherzoteller.midigen.processing;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * P3D can allow lines with Z: https://processing.org/reference/size_.html
 */
public class ProcessingApp extends UndecoratedMovableApplet {
	public Queue<String> numberQueue = new ConcurrentLinkedQueue<>();
	public static String lastNumber = "";
	private static ProcessingApp INSTANCE;
	private long lastLineTs = 0l;
	
	public ProcessingApp() {
		if(INSTANCE != null){
			throw new IllegalStateException("Only one instance allowed");
		}
		INSTANCE = this;
	}
	
	public static ProcessingApp getInstance(){
		return INSTANCE;
	}
	
	@Override
	public void settings() {
		size(1600, 1024);
	}

	@Override
	public void setup() {
		background(0x19,0x01,0x43);
	}

	private final float gaussian(float midleValue, float standardDeviation) {
		// Scale the gaussian random number by standard deviation and mean
		return (randomGaussian() * standardDeviation) + midleValue;
	}
	public static boolean addNumber(String number){
		if(INSTANCE == null) return false;
		if(!lastNumber.equals(number)){
			ProcessingApp.getInstance().numberQueue.add(number);
			lastNumber = number;
			return true;
		}else{
			return false;
		}
	}
	@Override
	public void draw() {
		long curTs = System.currentTimeMillis();
		if(curTs - lastLineTs > 2000l){
			blurredLine(random(width));
			lastLineTs = curTs;
		}
		if(!numberQueue.isEmpty()){
			String number = numberQueue.poll();
			if(number != null){
				float centerX = gaussian(width/2, 30);
				float centerY = gaussian(height/2, 30);
				
				float txtWidth = 150;
				float txtHeight = 30;
				{
					float textPos = centerX - (txtWidth/2);
					float textPosY = centerY - (txtHeight/2);
					stroke(0);
					fill(0, 190);
					ellipse(centerX , centerY, (150/2)*10, (30/2)*10);
					stroke(255);
					fill(255);
					textSize(25);
					text(number, textPos, textPosY, textPos + 150, 30);
				}
			}else{
				println("Queue empty");
			}
		}
	}
	public void blurredLine(float x){
		for(int i=0;i<10;i++){
			float realX = gaussian(x, 1);
			stroke(0xf3, 0x9b, 0xf9, 50);
			line(realX, 0, realX, height);
		}
	}
}