package org.scherzoteller.midigen.utils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class Recorder {
	
	
    // record duration, in milliseconds
    static final long RECORD_TIME = 60000;  // 1 minute
 
//    // path of the wav file
    File wavFile ;
 
    // format of audio file
    AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
 
    // the line from which audio data is captured
    TargetDataLine line;
 
    /**
     * Defines an audio format
     */
    AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 2;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                                             channels, signed, bigEndian);
        return format;
    }
 
    /**
     * Captures the sound and record into a WAV file
     */
    void start(Runnable r) {
        try {
            AudioFormat format = getAudioFormat();
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
 
            // checks if system supports the data line
            if (!AudioSystem.isLineSupported(info)) {
                System.out.println("Line not supported");
                System.exit(0);
            }
            line = (TargetDataLine) AudioSystem.getLine(info);
            
//            AudioSystem.getMixer(info)
            
            line.open(format);
            line.start();   // start capturing
 
            System.out.println("Start capturing...");
 
            AudioInputStream ais = new AudioInputStream(line);
 
            System.out.println("Start recording...");
            
//            // The stuff that does some music
//            r.run();
            
 
            // start recording
            AudioSystem.write(ais, fileType, wavFile);
 
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
 
    /**
     * Closes the target data line to finish capturing and recording
     */
    void finish() {
        line.stop();
        line.close();
        System.out.println("Finished");
    }
 
    private boolean finished = false;
    
    public synchronized boolean isFinished() {
		return finished;
	}
    

	public synchronized void finished() {
		this.finished = true;
	}

	public Recorder(String wavFile, Runnable r) {
		this.wavFile = new File(wavFile);
        // creates a new thread that waits for a specified
        // of time before stopping
        Thread stopper = new Thread(new Runnable() {
            public void run() {
            	while(!isFinished()){
	                try {
	                    Thread.sleep(500);
	                } catch (InterruptedException ex) {
	                    ex.printStackTrace();
	                }
            	}
                Recorder.this.finish();
            }
        });
 
        stopper.start();
 
        // start recording
        new Thread(() -> {
        	this.start(r);
        }, "Recorder!").start();
        r.run();
        finished();
	}
    
}
